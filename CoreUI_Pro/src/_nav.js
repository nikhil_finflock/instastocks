export default {
  items: [
    {
      name: 'Dashboard',
      url: '/home/dashboard',
      icon: 'icon-speedometer'
    },
    {
      title: true,
      name: 'Economics',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Analysis',
      url: '/home/economics/analysis',
      icon: 'icon-note'
    },
    {
      title: true,
      name: 'Equity',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      title: true,
      name: 'Fundamental Analysis',
      class: 'ml-3',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Summary',
      url: '/home/fundamental/analysis',
      icon: 'icon-note'
    },
    {
      name: 'Comparison',
      url: '/home/fundamental/compare',
      icon: 'icon-list'
    },
    {
      name: 'Scanner',
      // url: '/home/fundamental/scanner',
      url: '/info',
      icon: 'icon-list'
    },
    {
      title: true,
      name: 'Technical Analysis',
      class: 'ml-3',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Indicators',
      url: '/home/technical/indicators',
      icon: 'icon-note'
    },
    {
      name: 'Scanner',
      url: '/home/technical/scanner',
      icon: 'fa fa-bar-chart'
    },
    {
      name: 'Chart',
      url: '/home/technical/chart',
      icon: 'fa fa-bar-chart'
    },
    {
      title: true,
      name: 'Predictive Analysis',
      class: 'ml-3',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Projection',
      url: '/home/predictive/projection',
      icon: 'icon-note'
    },
    {
      name: 'News Sentiment',
      url: '/home/predictive/sentiment',
      icon: 'icon-note'
    },
    
        
    {
      title: true,
      name: 'Backtesting',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    // {
    //   name: 'Strategy Backtester',
    //   url: '/home/backtesting/strategy',
    //   icon: 'icon-calculator'
    // },
    {
      name: 'Single Stock',
      url: '/home/backtesting/single_stock',
      icon: 'icon-calculator'
    },
    {
      name: 'Portfolio Backtester',
      // url: '/home/backtesting/portfolio',
      url: '/info',
      icon: 'icon-calculator'
    },
    {
      title: true,
      name: 'Portfolio',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Model Portfolio',
      url: '/home/portfolio_list',
      icon: 'icon-note'
    },
    {
      name: 'Live Portfolio',
      url: '/home/holding_portfolio',
      icon: 'icon-note'
    },
    
    {
      title: true,
      name: 'Sector Analysis',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Top Sector',
      url: '/home/marketData/sector',
      icon: 'fa fa-university'
    },
    {
      name: 'Top Industries',
      url: '/home/marketData/industry',
      icon: 'fa fa-industry'
    },

    {
      title: true,
      name: 'News',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Business Headlines',
      url: '/home/news/headlines',
      icon: 'icon-note'
    },
    // {
    //   title: true,
    //   name: 'Extra',
    //   class: '',
    //   wrapper: {
    //     element: '',
    //     attributes: {}
    //   }
    // },
    // {
    //   name: 'Widgets',
    //   url: '/home/extra/widgets',
    //   icon: 'fa fa-cube'
    // }


  ]
}


