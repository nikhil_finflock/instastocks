export default {
    items: [
      {
        name: 'Dashboard - IN',
        url: '/IN/dashboard',
        icon: 'icon-speedometer'
      },
      {
        title: true,
        name: 'Economics',
        class: '',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Analysis',
        // url: '/IN/economics/analysis',
        url: '/info',
        icon: 'icon-note'
      },
      {
        title: true,
        name: 'Equity',
        class: '',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        title: true,
        name: 'Fundamental Analysis',
        class: 'ml-3',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Summary',
        url: '/IN/fundamental/analysis',
        icon: 'icon-note'
      },
      {
        name: 'Comparison',
        url: '/IN/fundamental/compare',
        icon: 'icon-list'
      },
      {
        name: 'Scanner',
        // url: '/IN/fundamental/scanner',
        url: '/info',
        icon: 'icon-list'
      },
      {
        title: true,
        name: 'Technical Analysis',
        class: 'ml-3',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Indicators',
        url: '/IN/technical/indicators',
        icon: 'icon-note'
      },
      {
        name: 'Scanner',
        url: '/IN/technical/scanner',
        icon: 'fa fa-bar-chart'
      },
      // {
      //   name: 'Chart',
      //   url: '/IN/technical/chart',
      //   icon: 'fa fa-bar-chart'
      // },
      {
        title: true,
        name: 'Predictive Analysis',
        class: 'ml-3',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Projection',
        url: '/IN/predictive/projection',
        icon: 'icon-note'
      },
      {
        name: 'News Sentiment',
        url: '/IN/predictive/sentiment',
        icon: 'icon-note'
      },
      
          
      {
        title: true,
        name: 'Backtesting',
        class: '',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      // {
      //   name: 'Strategy Backtester',
      //   url: '/IN/backtesting/strategy',
      //   icon: 'icon-calculator'
      // },
      {
        name: 'Single Stock',
        url: '/IN/backtesting/single_stock',
        icon: 'icon-calculator'
      },
      {
        name: 'Portfolio Backtester',
        // url: '/IN/backtesting/portfolio',
        url: '/info',
        icon: 'icon-calculator'
      },
      {
        title: true,
        name: 'Portfolio',
        class: '',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Model Portfolio',
        url: '/IN/portfolio_list',
        icon: 'icon-note'
      },
      {
        name: 'Live Portfolio',
        url: '/IN/holding_portfolio',
        icon: 'icon-note'
      },
      
      {
        title: true,
        name: 'Sector Analysis',
        class: '',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Top Sector',
        url: '/IN/marketData/sector',
        icon: 'fa fa-university'
      },
      {
        name: 'Top Industries',
        url: '/IN/marketData/industry',
        icon: 'fa fa-industry'
      },
  
      {
        title: true,
        name: 'News',
        class: '',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Business Headlines',
        url: '/IN/news/headlines',
        icon: 'icon-note'
      },
      // {
      //   title: true,
      //   name: 'Extra',
      //   class: '',
      //   wrapper: {
      //     element: '',
      //     attributes: {}
      //   }
      // },
      // {
      //   name: 'Widgets',
      //   url: '/IN/extra/widgets',
      //   icon: 'fa fa-cube'
      // }
  
  
    ]
  }
  
  
  