export default {
    items: [
      {
        name: 'Dashboard - US',
        url: '/US/dashboard',
        icon: 'icon-speedometer'
      },
      {
        title: true,
        name: 'Economics',
        class: '',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Analysis',
        url: '/US/economics/analysis',
        icon: 'icon-note'
      },
      {
        title: true,
        name: 'Equity',
        class: '',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        title: true,
        name: 'Fundamental Analysis',
        class: 'ml-3',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Summary',
        url: '/US/fundamental/analysis',
        icon: 'icon-note'
      },
      {
        name: 'Comparison',
        url: '/US/fundamental/compare',
        icon: 'icon-list'
      },
      {
        name: 'Scanner',
        // url: '/US/fundamental/scanner',
        url: '/info',
        icon: 'icon-list'
      },
      {
        title: true,
        name: 'Technical Analysis',
        class: 'ml-3',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Indicators',
        url: '/US/technical/indicators',
        icon: 'icon-note'
      },
      {
        name: 'Scanner',
        url: '/US/technical/scanner',
        icon: 'fa fa-bar-chart'
      },
      // {
      //   name: 'Chart',
      //   url: '/US/technical/chart',
      //   icon: 'fa fa-bar-chart'
      // },
      {
        title: true,
        name: 'Predictive Analysis',
        class: 'ml-3',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Projection',
        url: '/US/predictive/projection',
        icon: 'icon-note'
      },
      {
        name: 'News Sentiment',
        url: '/US/predictive/sentiment',
        icon: 'icon-note'
      },
      
          
      {
        title: true,
        name: 'Backtesting',
        class: '',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      // {
      //   name: 'Strategy Backtester',
      //   url: '/US/backtesting/strategy',
      //   icon: 'icon-calculator'
      // },
      {
        name: 'Single Stock',
        url: '/US/backtesting/single_stock',
        icon: 'icon-calculator'
      },
      {
        name: 'Portfolio Backtester',
        // url: '/US/backtesting/portfolio',
        url: '/info',
        icon: 'icon-calculator'
      },
      {
        title: true,
        name: 'Portfolio',
        class: '',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Model Portfolio',
        url: '/US/portfolio_list',
        icon: 'icon-note'
      },
      {
        name: 'Live Portfolio',
        url: '/US/holding_portfolio',
        icon: 'icon-note'
      },
      
      {
        title: true,
        name: 'Sector Analysis',
        class: '',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Top Sector',
        url: '/US/marketData/sector',
        icon: 'fa fa-university'
      },
      {
        name: 'Top Industries',
        url: '/US/marketData/industry',
        icon: 'fa fa-industry'
      },
  
      {
        title: true,
        name: 'News',
        class: '',
        wrapper: {
          element: '',
          attributes: {}
        }
      },
      {
        name: 'Business Headlines',
        url: '/US/news/headlines',
        icon: 'icon-note'
      },
      // {
      //   title: true,
      //   name: 'Extra',
      //   class: '',
      //   wrapper: {
      //     element: '',
      //     attributes: {}
      //   }
      // },
      // {
      //   name: 'Widgets',
      //   url: '/US/extra/widgets',
      //   icon: 'fa fa-cube'
      // }
  
  
    ]
  }
  
  
  