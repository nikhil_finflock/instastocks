// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import './polyfill'
// import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router/index'
import HighchartsVue from 'highcharts-vue'
import Highcharts from 'highcharts'
import stockInit from 'highcharts/modules/stock'
import exportingInit from "highcharts/modules/exporting";
// import stockTools from "highcharts/modules/stock-tools";

import VueFusionCharts from 'vue-fusioncharts';
import FusionCharts from 'fusioncharts';
import candyTheme from 'fusioncharts/themes/fusioncharts.theme.candy';
import AngularGauge from 'fusioncharts/fusioncharts.widgets';

import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
import BackToTop from 'vue-backtotop'
import VueCookies from 'vue-cookies'
import VueTimers from 'vue-timers'

// todo
// cssVars()

exportingInit(Highcharts)
stockInit(Highcharts)
// stockTools(Highcharts)

Vue.use(BootstrapVue)

Vue.use(HighchartsVue)

Vue.use(BootstrapVue)

Vue.use(VueFusionCharts, FusionCharts, AngularGauge, candyTheme);

Vue.use(VueChartkick, {adapter: Chart})

Vue.use(BackToTop)

Vue.use(VueCookies)
// set default config
VueCookies.config('14d')

Vue.use(VueTimers)

// set global cookie
VueCookies.set('theme','default');
VueCookies.set('hover-time','1s');

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  }
})
