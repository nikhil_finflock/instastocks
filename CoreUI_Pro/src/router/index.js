import Vue from 'vue'
import Router from 'vue-router'

// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')

// Authentication
const login = () => import('@/auth/login')
const register = () => import('@/auth/register')
const PageNotFound  = () => import('@/auth/404')
const info  = () => import('@/auth/info')
const forget  = () => import('@/auth/forget')

// Views
const Dashboard = () => import('@/views/Dashboard')
const Analysis = () => import('@/views/Analysis')
const Compare = () => import('@/views/Compare')
const techAnalysis = () => import('@/views/techAnalysis')
const industry = () => import('@/views/industry')
const strategy_back = () => import('@/views/strategy_back')
const headlines = () => import('@/views/headlines')
const widgets = () => import('@/views/widgets')
const Economic_Analysis2 = () => import('@/views/Economic_Analysis2')
const Portfolio_Analysis = () => import('@/views/Portfolio_Analysis')
const Portfolio = () => import('@/views/Portfolio')
const scanner = () => import('@/views/scanner')
const sector = () => import('@/views/sector')
const valuation = () => import('@/views/valuation')
const stockList = () => import('@/views/stockList')
const stockAll = () => import('@/views/stock_all')
const projection = () => import('@/views/projection')
const news_sentiment = () => import('@/views/news_sentiment')
const tech_chart = () => import('@/views/tech_chart')
const full_chart = () => import('@/views/full_chart')
const holding_portfolio = () => import('@/views/holding_portfolio') 
const ib_portfolio = () => import('@/views/ib_portfolio')
const strategies = () => import('@/views/strategy_view')

const containerIN = () => import('@/views/IN_dashboard/containerIN')
const containerUS = () => import('@/views/US_dashboard/containerUS')

// const Economic_Strategies = () => import('@/views/Economic_Strategies')

Vue.use(Router)

export default new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/home',
      redirect: '/home/dashboard',
      name: 'Home',
      component: DefaultContainer,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'fundamental',
          redirect: '/fundamental/analysis',
          name: 'Fundamental',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'analysis',
              name: 'Analysis',
              component: Analysis
            },
            {
              path: 'compare',
              name: 'Compare',
              component: Compare
            }
          ]
        },
        {
          path: 'technical',
          redirect: '/technical/indicators',
          name: 'Technical',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'indicators',
              name: 'Indicators',
              component: techAnalysis
            },
            {
              path: 'scanner',
              name: 'Scanner',
              component: scanner
            },
            {
              path: 'chart',
              name: 'Chart',
              component: tech_chart
            }
          ]
        },
        {
          path: 'predictive',
          redirect: '/predictive/projection',
          name: 'Predictive Analysis',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'projection',
              name: 'Projection Analysis',
              component: projection
            },
            {
              path: 'sentiment',
              name: 'Sentiment Analysis',
              component: news_sentiment
            }
          ]
        },
        {
          path: 'economics',
          redirect: '/backtesting/analysis',
          name: 'Backtesting',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'analysis',
              name: 'Economic Analysis',
              component: Economic_Analysis2
            }

            // {
            //   path: 'strategy',
            //   name: 'Economic Strategies',
            //   component: Economic_Strategies
            // }
          ]
        },
        {
          path: 'backtesting',
          redirect: '/backtesting/strategy',
          name: 'Backtesting',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'strategy',
              name: 'Strategy Backtester',
              component: strategy_back
            },
            {
              path: 'single_stock',
              name: 'Single Stock Backtester',
              component: valuation
            },
            {
              path: 'strategy',
              name: 'Strategy Backtester',
              component: strategy_back
            }
          ]
        },

        {
          path: 'marketData',
          redirect: '/marketData/scanner',
          name: 'Market Data',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'sector',
              name: 'Sector',
              component: sector
            },
            {
              path: 'industry',
              name: 'Top 10 Industries',
              component: industry
            }
            
          ]
        },
        {
          path: 'news',
          redirect: '/news/headlines',
          name: 'News',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'headlines',
              name: 'Headlines',
              component: headlines
            }
            
          ]
        },
        {
          path: 'extra',
          redirect: '/extra/widgets',
          name: 'Extra',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'widgets',
              name: 'Widgets',
              component: widgets
            }
            
          ]
        },
        // {
        //   path: 'portfolio',
        //   redirect: '/portfolio/analysis',
        //   name: 'Portfolio',
        //   component: {
        //     render (c) { return c('router-view') }
        //   },
        //   children: [
        //     {
        //       path: 'analysis',
        //       name: 'Analysis',
        //       component: Portfolio_Analysis
        //     }

        //   ]  
        // },
        {
          path: '/portfolio/:id',
          name: 'Portfolio',
          component: Portfolio_Analysis
        },
        {
          path: 'portfolio_list',
          name: 'Portfolio List',
          component: Portfolio
        },
        {
          path: 'holding_portfolio',
          name: 'Holding Portfolio',
          component: holding_portfolio
        },
        {
          path: 'ib_portfolio',
          name: 'IB Portfolio',
          component: ib_portfolio
        },

        {
          path: '/StockListByName/:name',
          name: 'Stock List By Name',
          component: stockList
        },
        {
          path: '/stock/:name',
          name: 'Stock',
          component: stockAll
        },
        {
          path: 'strategies',
          name: 'Strategies',
          component: strategies
        }

        
        
  
       
      ]
    },



    {
      path: '/US',
      redirect: '/US/dashboard',
      name: 'US',
      component: containerUS,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'fundamental',
          redirect: '/fundamental/analysis',
          name: 'Fundamental',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'analysis',
              name: 'Analysis',
              component: Analysis
            },
            {
              path: 'compare',
              name: 'Compare',
              component: Compare
            }
          ]
        },
        {
          path: 'technical',
          redirect: '/technical/indicators',
          name: 'Technical',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'indicators',
              name: 'Indicators',
              component: techAnalysis
            },
            {
              path: 'scanner',
              name: 'Scanner',
              component: scanner
            },
            {
              path: 'chart',
              name: 'Chart',
              component: tech_chart
            }
          ]
        },
        {
          path: 'predictive',
          redirect: '/predictive/projection',
          name: 'Predictive Analysis',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'projection',
              name: 'Projection Analysis',
              component: projection
            },
            {
              path: 'sentiment',
              name: 'Sentiment Analysis',
              component: news_sentiment
            }
          ]
        },
        {
          path: 'economics',
          redirect: '/backtesting/analysis',
          name: 'Backtesting',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'analysis',
              name: 'Economic Analysis',
              component: Economic_Analysis2
            }

            // {
            //   path: 'strategy',
            //   name: 'Economic Strategies',
            //   component: Economic_Strategies
            // }
          ]
        },
        {
          path: 'backtesting',
          redirect: '/backtesting/strategy',
          name: 'Backtesting',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'strategy',
              name: 'Strategy Backtester',
              component: strategy_back
            },
            {
              path: 'single_stock',
              name: 'Single Stock Backtester',
              component: valuation
            },
            {
              path: 'strategy',
              name: 'Strategy Backtester',
              component: strategy_back
            }
          ]
        },

        {
          path: 'marketData',
          redirect: '/marketData/scanner',
          name: 'Market Data',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'sector',
              name: 'Sector',
              component: sector
            },
            {
              path: 'industry',
              name: 'Top 10 Industries',
              component: industry
            }
            
          ]
        },
        {
          path: 'news',
          redirect: '/news/headlines',
          name: 'News',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'headlines',
              name: 'Headlines',
              component: headlines
            }
            
          ]
        },
        {
          path: 'extra',
          redirect: '/extra/widgets',
          name: 'Extra',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'widgets',
              name: 'Widgets',
              component: widgets
            }
            
          ]
        },
        // {
        //   path: 'portfolio',
        //   redirect: '/portfolio/analysis',
        //   name: 'Portfolio',
        //   component: {
        //     render (c) { return c('router-view') }
        //   },
        //   children: [
        //     {
        //       path: 'analysis',
        //       name: 'Analysis',
        //       component: Portfolio_Analysis
        //     }

        //   ]  
        // },
        {
          path: '/portfolio/:id',
          name: 'Portfolio',
          component: Portfolio_Analysis
        },
        {
          path: 'portfolio_list',
          name: 'Portfolio List',
          component: Portfolio
        },
        {
          path: 'holding_portfolio',
          name: 'Holding Portfolio',
          component: holding_portfolio
        },
        {
          path: 'ib_portfolio',
          name: 'IB Portfolio',
          component: ib_portfolio
        },

        {
          path: '/StockListByName/:name',
          name: 'Stock List By Name',
          component: stockList
        },
        {
          path: '/stock/:name',
          name: 'Stock',
          component: stockAll
        }

        
        
  
       
      ]
    },
    {
      path: '/IN',
      redirect: '/IN/dashboard',
      name: 'IN',
      component: containerIN,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'fundamental',
          redirect: '/fundamental/analysis',
          name: 'Fundamental',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'analysis',
              name: 'Analysis',
              component: Analysis
            },
            {
              path: 'compare',
              name: 'Compare',
              component: Compare
            }
          ]
        },
        {
          path: 'technical',
          redirect: '/technical/indicators',
          name: 'Technical',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'indicators',
              name: 'Indicators',
              component: techAnalysis
            },
            {
              path: 'scanner',
              name: 'Scanner',
              component: scanner
            },
            {
              path: 'chart',
              name: 'Chart',
              component: tech_chart
            }
          ]
        },
        {
          path: 'predictive',
          redirect: '/predictive/projection',
          name: 'Predictive Analysis',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'projection',
              name: 'Projection Analysis',
              component: projection
            },
            {
              path: 'sentiment',
              name: 'Sentiment Analysis',
              component: news_sentiment
            }
          ]
        },
        {
          path: 'economics',
          redirect: '/backtesting/analysis',
          name: 'Backtesting',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'analysis',
              name: 'Economic Analysis',
              component: Economic_Analysis2
            }

            // {
            //   path: 'strategy',
            //   name: 'Economic Strategies',
            //   component: Economic_Strategies
            // }
          ]
        },
        {
          path: 'backtesting',
          redirect: '/backtesting/strategy',
          name: 'Backtesting',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'strategy',
              name: 'Strategy Backtester',
              component: strategy_back
            },
            {
              path: 'single_stock',
              name: 'Single Stock Backtester',
              component: valuation
            },
            {
              path: 'strategy',
              name: 'Strategy Backtester',
              component: strategy_back
            }
          ]
        },

        {
          path: 'marketData',
          redirect: '/marketData/scanner',
          name: 'Market Data',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'sector',
              name: 'Sector',
              component: sector
            },
            {
              path: 'industry',
              name: 'Top 10 Industries',
              component: industry
            }
            
          ]
        },
        {
          path: 'news',
          redirect: '/news/headlines',
          name: 'News',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'headlines',
              name: 'Headlines',
              component: headlines
            }
            
          ]
        },
        {
          path: 'extra',
          redirect: '/extra/widgets',
          name: 'Extra',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'widgets',
              name: 'Widgets',
              component: widgets
            }
            
          ]
        },
        // {
        //   path: 'portfolio',
        //   redirect: '/portfolio/analysis',
        //   name: 'Portfolio',
        //   component: {
        //     render (c) { return c('router-view') }
        //   },
        //   children: [
        //     {
        //       path: 'analysis',
        //       name: 'Analysis',
        //       component: Portfolio_Analysis
        //     }

        //   ]  
        // },
        {
          path: '/portfolio/:id',
          name: 'Portfolio',
          component: Portfolio_Analysis
        },
        {
          path: 'portfolio_list',
          name: 'Portfolio List',
          component: Portfolio
        },
        {
          path: 'holding_portfolio',
          name: 'Holding Portfolio',
          component: holding_portfolio
        },
        {
          path: 'ib_portfolio',
          name: 'IB Portfolio',
          component: ib_portfolio
        },

        {
          path: '/StockListByName/:name',
          name: 'Stock List By Name',
          component: stockList
        },
        {
          path: '/stock/:name',
          name: 'Stock',
          component: stockAll
        }

        
        
  
       
      ]
    },
    {
      path: '/',
      redirect: {
          name: "Login"
      }
  },
    {
      path: '/login',
      name: 'Login',
      component: login
    },
    {
    path: '/register',
      name: 'Register',
      component: register
    },
    {
      path: '/forget',
        name: 'Forget Password',
        component: forget
      },
      {
        path: '/info',
          name: 'Info',
          component: info
        },
        {
          path: '/full_chart/:name',
          name: 'Full Featured Chart',
          component: full_chart
        },

    { path: "*", component: PageNotFound }


  ]
})
