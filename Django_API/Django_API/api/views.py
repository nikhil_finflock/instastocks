import asyncio
import datetime
import hashlib
import json
import pickle
## Importing Libraries
import sys
import time
import warnings
from copy import deepcopy
from datetime import date, timedelta

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import quandl
import redis
import requests
from bokeh.models import ColumnDataSource
from bokeh.plotting import figure, output_file, show
# from bson import json_util
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from flask_cors import CORS
from ib_insync import *
from nsepy import get_history
from scipy.stats import zscore
from sklearn import linear_model, preprocessing
from sklearn.decomposition import PCA, FastICA, TruncatedSVD
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.metrics import (accuracy_score, confusion_matrix, f1_score,
                             precision_score, recall_score)
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import BernoulliRBM
from sklearn.random_projection import (GaussianRandomProjection,
                                       SparseRandomProjection)
from statsmodels.graphics.tsaplots import plot_pacf
from ta import *
import ta
import sqlite3
from pandas.io import sql
from pandas_datareader import data, wb
import pyodbc
from xgboost import XGBClassifier
from nsetools import Nse
import time
import datetime
from copy import deepcopy
from django.views.decorators.csrf import csrf_exempt 
from ib_insync import *
import ibapi
from ib_insync.contract import Stock, Forex, Index, Option, Future, CFD
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from fredapi import Fred
from yahoo_finance import Share
from yahoofinancials import YahooFinancials
# from django.contrib.auth import authenticate
# from django.views.decorators.csrf import csrf_exempt
# from rest_framework.authtoken.models import Token
# from rest_framework.decorators import api_view, permission_classes
# from rest_framework.permissions import AllowAny
# from rest_framework.status import (
#     HTTP_400_BAD_REQUEST,
#     HTTP_404_NOT_FOUND,
#     HTTP_200_OK
# )
# from rest_framework.response import Response


##CACHE
# def index(request):
#     r = redis.StrictRedis(host='localhost', port=6379, db=0)
#     select = request.GET.get('select')
#     fro = request.GET.get('fro')
#     where = request.GET.get('where')
#     print(select , fro , where)
#     cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
#     cursor = cnxn.cursor()
#     select = str(select)
#     fro = str(fro)
#     where = str(where)
#     where = where.replace('-' , '=')
#     SQL_String = "SELECT " + select + " FROM " + fro + " WHERE " + where
#     print("THIS IS SQL QUERY : " , SQL_String)

#     hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
#     print(hash)
#     # r.delete(hash)
#     if(r.get(hash)):
#         print("LOADED FROM CACHE")
#         StartTime = time.time()
#         b = pd.read_sql_query(SQL_String , cnxn)
#         print(b)
#         EndTime = time.time()
#         TotalTime = EndTime - StartTime
#     else:
#         print("LOADED FROM DB")
#         StartTime = time.time()

#         b = pd.read_sql_query(SQL_String , cnxn)


#     # b = pd.read_sql_query(SQL_String , cnxn)
#     jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
#     data = b.to_json()
#     # a = [[1,2,3,4,5,6,7], [1,2,3,4,5,6,7]]
#     return HttpResponse(jsonfiles,  content_type="application/json")
#     # return JsonResponse(a, safe=False)


# http://127.0.0.1:8000/api/?select=Date,Ratio_Name,Ratio_Value&fro=Indian_Fundamental_Ratios&where=%27TCS%27
def index(request):
    select = request.GET.get('select')
    fro = request.GET.get('fro')
    where = request.GET.get('where')
    print(select , fro , where)
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    select = str(select)
    fro = str(fro)
    where = str(where)
    where = where.replace('-' , '=')
    SQL_String = "SELECT " + select + " FROM " + fro + " WHERE " + where
    print("THIS IS SQL QUERY : " , SQL_String)
    b = pd.read_sql_query(SQL_String , cnxn)
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def niftycharts(request):
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    code_niftycharts = "NiftyCharts"
    hash_niftycharts = hashlib.sha256(code_niftycharts.encode('utf-8')).hexdigest()


    if(r.get(hash_niftycharts)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash_niftycharts))
    else:
        print("LOADED FROM DB")
            
        d1 = date.today()
        d2 = date.today() - timedelta(days=100)
        N50 = get_history(symbol='NIFTY 50' ,start=d2,end=d1,index=True)
        N50I = get_history(symbol='NIFTY IT' ,start=d2,end=d1,index=True)
        N50A = get_history(symbol='NIFTY AUTO' ,start=d2,end=d1,index=True)
        N50B = get_history(symbol='NIFTY BANK' ,start=d2,end=d1,index=True)
        INDIAVIX = get_history(symbol='NIFTY MEDIA' ,start=d2,end=d1,index=True)
        CNX100 = get_history(symbol='NIFTY METAL' ,start=d2,end=d1,index=True)
        NI15 = get_history(symbol='NIFTY 200' ,start=d2,end=d1,index=True)
        CNXIT = get_history(symbol='NIFTY 500' ,start=d2,end=d1,index=True)

        print(N50)

        

        NI15
        N50 = N50.iloc[-60:]
        N50I = N50I.iloc[-60:]
        N50A =N50A.iloc[-60:]
        N50B = N50B.iloc[-60:]

        INDIAVIX = INDIAVIX.iloc[-60:]
        CNX100 = CNX100.iloc[-60:]
        NI15 =NI15.iloc[-60:]
        CNXIT = CNXIT.iloc[-60:]
        N50['Date'] = N50.index
        a = list(N50['Close'])
        b = list(N50I['Close'])
        c = list(N50A['Close'])
        d = list(N50B['Close'])

        aa = list(INDIAVIX['Close'])
        bb = list(CNX100['Close'])
        cc = list(NI15['Close'])
        dd = list(CNXIT['Close'])

        e = N50['Close'].iloc[-1]
        f = str(N50I['Close'].iloc[-1])
        g = str(N50A['Close'].iloc[-1])
        l = str(N50B['Close'].iloc[-1])

        ee = INDIAVIX['Close'].iloc[-1]
        ff = str(CNX100['Close'].iloc[-1])
        gg = str(NI15['Close'].iloc[-1])
        ll = str(CNXIT['Close'].iloc[-1])

        j = []
        for i in range(0, N50.shape[0]):
            j.append(str(N50['Date'].iloc[i]))
        LIST = [{'Nifty50Close' : a} , {'NiftyITClose' : b} , {'NiftyAutoClose' : c} ,  {'NiftyBankClose' : d}, {'NIFTYMEDIAClose' : aa} , {'NIFTYMETALClose' : bb} , {'NIFTY200Close' : cc} ,  {'NIFTY500Close' : dd} , {'Nifty50Close1' : e} ,  {'NiftyITClose1' : f} ,  {'NiftyAutoClose1' : g} ,  {'NiftyBankClose1' : l},{'NIFTYMEDIAClose1' : ee} ,  {'NIFTYMETALClose1' : ff} ,  {'NIFTY200Close1' : gg} ,  {'NIFTY500Close1' : ll}  ,  {'Date' : j}]


        pickobj = pickle.dumps(LIST)
        r.set(hash_niftycharts , pickobj)
        r.expire(hash_niftycharts , 60*60*24)
        LIST  = pickle.loads(r.get(hash_niftycharts))      
    
    # print(N50)
    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def ib(request):
    All_List = []
    asyncio.set_event_loop(asyncio.new_event_loop())
    ib = IB()
    ib.connect('127.0.0.1', 7497, clientId=100)
    a = ib.reqPositions()
    for i in a:
        All_List.append([i.account , i.contract.secType  , i.contract.conId , i.contract.symbol , i.contract.currency , i.contract.localSymbol , i.contract.tradingClass , i.position , i.avgCost])
    
    b = pd.DataFrame(All_List)
    
    b.columns = ['Account' , 'SecType' , 'ID' , 'Symbol' , 'Currency' , 'LocalSymbol' , 'TradingClass' , 'Position' , 'BuyPrice']
   
    b['CurrentPrice'] = 0
    b['Change'] = 0
    # b['Change (%)'] = 0
    b['P&L'] = 0
    for i in range(0, b.shape[0]):
        try:
            if (b['SecType'][i]=="STK"):

                c = Stock(b['Symbol'][i], 'NSE', 'INR')
                ib.reqMarketDataType(4)
                ticker = ib.reqMktData(c, '', False, False, None)
                ib.sleep(1)
                # print(ticker)
                # print(ticker.last)
                # print((ticker.last - b['BuyPrice'][0]))
                # print(((ticker.last*b['Position'][i] - b['BuyPrice'][i]*b['Position'][i])))

                b['CurrentPrice'][i] = ticker.last
                b['Change'][i] = (ticker.last - b['BuyPrice'][0])
                # b['Change (%)'][i] = (((ticker.last - x['BuyPrice'][0]))/x['BuyPrice'][0])*100
                b['P&L'][i] = ((ticker.last*b['Position'][i] - b['BuyPrice'][i]*b['Position'][i]))
        except:
            pass
    b = b.to_json(orient='records')
    ib.disconnect()
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")




def forex(request):
    asyncio.set_event_loop(asyncio.new_event_loop())
    ib = IB()
    ib.connect('127.0.0.1', 7497, clientId=20)
    contract = Contract()
    contract.symbol = 'TCS'
    contract.secType = "STK"
    contract.currency = "INR"
    contract.exchange = "NSE"
    contract.primaryExchange = "NSE"

    queryTime = datetime.datetime.today()
    a = ib.reqHistoricalData(contract, queryTime,"2000 S", "30 secs", "MIDPOINT", 1, 1, False, [])
    To_Analyse = []
    for ticks in a:
        To_Analyse.append(ticks.open)
    b = pd.DataFrame(To_Analyse)
    ib.disconnect()
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    data = b.to_json()
    return HttpResponse(jsonfiles,  content_type="application/json")


def TimeSeries(request):
    region = request.GET.get('region')
    region = region.upper()
    name = request.GET.get('name')
    name = name.upper()
    queryTime = datetime.datetime.today()
    quandl.ApiConfig.api_key = 'Yc9EU38DA1ry-YHirE6Y'
    if(region == 'IN'):
        ind = "NSE/"
    elif(region == 'US'):
        ind = "WIKI/"
    st = ind + str(name)
    a = quandl.get(st, start_date='2015-1-1', end_date=queryTime)
    a['Date'] = a.index
    jsonfiles = json.dumps(json.loads(a.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


## For HighCharts, Single Stock OHLC
def OHLC(request):
    List = []
    name = request.GET.get('name')
    print("THIS IS NAME" , name)
    queryTime = datetime.datetime.today()
    quandl.ApiConfig.api_key = 'Yc9EU38DA1ry-YHirE6Y'
    st = "NSE/" + str(name)
    a = quandl.get(st, start_date='2000-1-1', end_date=queryTime)
    a['Date'] = a.index
    a['ts'] = a.Date.values.astype(np.int64)
    a['ts'] = a['ts']//1000000
    print(a.tail())
    for index, row in a.iterrows():
        List.append([ row['ts'] , row['Open'] , row['High'], row['Low'], row['Close'] ])
    
    List = json.dumps(List  , indent=4 , cls=DjangoJSONEncoder)
    return HttpResponse(List,  content_type="application/json")

def OHLCV(request):
    List = []
    name = request.GET.get('name')
    print("THIS IS NAME" , name)
    queryTime = datetime.datetime.today()
    quandl.ApiConfig.api_key = 'Yc9EU38DA1ry-YHirE6Y'
    st = "NSE/" + str(name)
    a = quandl.get(st, start_date='2000-1-1', end_date=queryTime)
    # print(a)
    a['Date'] = a.index
    a['ts'] = a.Date.values.astype(np.int64)
    a['ts'] = a['ts']//1000000
    print(a.tail())
    for index, row in a.iterrows():
        List.append([ row['ts'] , row['Open'] , row['High'], row['Low'], row['Close'], row['Total Trade Quantity'] ])
    
    List = json.dumps(List  , indent=4 , cls=DjangoJSONEncoder)
    return HttpResponse(List,  content_type="application/json")


## For Stock Quotes
def quotes(request):
    List = []
    name = request.GET.get('name')
    d1 = date.today() - timedelta(days=2)
    b = get_history(symbol=name, start=date(2015,1,10), end=date(2025,1,10))
    b = b.tail(1)
    b['Change'] = ((b['Last'] - b['Prev Close'])/b['Last'])*100
    b['ChangeValue'] = ((b['Last'] - b['Prev Close']))
    b['Change'] = b['Change'].round(2)
    b['Prev_Close'] = b['Prev Close']
    b['Deliverable_Volume'] = b['Deliverable Volume']
    b.drop(['Deliverable Volume' , 'Prev Close'] , axis=1 , inplace=True)
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    data = b.to_json()
    return HttpResponse(jsonfiles,  content_type="application/json")


## For HighCharts, Compare Multiple Stock (Close only)
def Compare(request):
    Final = []
    List = []
    stocks = request.GET.get('stocks')
    stocks = stocks.split(',')
    queryTime = datetime.datetime.today()
    quandl.ApiConfig.api_key = 'Yc9EU38DA1ry-YHirE6Y'
    for i in stocks:
        st = "NSE/" + str(i)
        a = quandl.get(st, start_date='2000-1-1', end_date=queryTime)
        # a = quandl.get(st, start_date='2000-1-1', end_date='2001-1-1')
        a['Date'] = a.index
        a['ts'] = a.Date.values.astype(np.int64)
        a['ts'] = a['ts']//1000000
        print(a.head())
        for index, row in a.iterrows():
            List.append([ row['ts'] , row['Close'] ])
        dictt  = {'name' : i , 'data' : List}
        Final.append(dictt)
        # print(Final)
        List = []
    
    # List = json.dumps(Final  , indent=4, default=json_util.default)
    # return HttpResponse(Final,  content_type="application/json")

    a = json.dumps(Final  , indent=4)
    return HttpResponse(a,  content_type="application/json")



def Fundamental_Ratios(request):
    select = request.GET.get('select')
    fro = request.GET.get('fro')
    where = request.GET.get('where')
    print(select , fro , where)
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    select = str(select)
    fro = str(fro)
    where = str(where)
    where = where.replace('-' , '=')
    SQL_String = "SELECT " + select + " FROM " + fro + " WHERE " + where
    print("THIS IS SQL QUERY : " , SQL_String)
    b = pd.read_sql_query(SQL_String , cnxn)
    b = b.loc[b['Date'] == str(max(b['Date'])) ]

    df2 = pd.read_csv('./Fundamental_Ratios_desc.csv')
    print(df2.head())
    b = pd.merge(b , df2 , on='Ratio_Name')
    b = b[['Ratio_Desc' , 'Ratio_Value']]
    print(b.head(0))
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    data = b.to_json()
    # a = [[1,2,3,4,5,6,7], [1,2,3,4,5,6,7]]
    return HttpResponse(jsonfiles,  content_type="application/json")
    # return JsonResponse(a, safe=False)


def ml_models(request):
    a = request.GET.get('Symbol')
    b = request.GET.get('drop')
    c = request.GET.get('model')
    print(a)
    print(b)
    print(c)
    query = "http://www.finflock.com/api/?select=End_Date,Abbreviation_Value&fro="+ b +"_Fundamentals_Financial_Statements&where=Company_Symbol='"+ a +"' AND Abbreviation='"+ c +"' AND Fiscal_Period_Type='Annual'"
    print(query)
    html = requests.get(query).content
    df = pd.read_json(html)
    print(df)

    df = df.sort_values('End_Date', ascending=True)
    df.reset_index(inplace=True)
    df = df[['End_Date' , 'Abbreviation_Value']]

    # table = [ {'End_Date' : list(df['End_Date'])} , {'Abbreviation_Value' : list(df['Abbreviation_Value'])  }  ]  
    # table = json.dumps(table)
    # table = df.to_json(orient='records')

    model = LinearRegression()
    model.fit(pd.DataFrame(df.index), pd.DataFrame(df['Abbreviation_Value']))
    m= max(df.index)  + 1
    print("THIS IS INDEX" , df.index)
    scatter = list(df['Abbreviation_Value'])
    
    predict = model.predict(m)[0][0]
    initial = model.predict(0)[0][0]
    predict = round(predict, 2)
    initial = round(initial, 2)

    query2 = "http://www.finflock.com/api/?select=Company,ISIN_No,Code,Industry&fro=All_Stocks_Symbol_Data&where=Symbol='"+ a +str("'")
    html = requests.get(query2).content
    info = pd.read_json(html)

    print(info.head())
    company = info.ix[0]['Company']
    isin = info.ix[0]['ISIN_No']
    code = info.ix[0]['Code']
    industry = str(info.ix[0]['Industry'])
    industry = industry.replace("amp;" , "")
    print(company , isin , code , industry)
    print(info)
    # LIST = [{'a' : a }, {'b' : b }, {'c' : c}, {'zipdf' : zipdf}, {'sym': MovingLine_Symbols} ,{ 'gain': MovingLine_Gain}, {'lose': MovingLine_Lose} , {'predict': predict}, {'initial': initial}, {'scatter': scatter}, {'m': m}, {'company': company}, {'isin': isin}, {'code': code}, {'industry': industry}]
    LIST = [{'company': str(company) }, {'isin': str(isin) }, {'code': str(code) }, {'industry': str(industry)}, {'scatter': scatter}, { 'm': m }, { 'predict': predict }, {'initial': initial}, {'table': df.to_json(orient='records') } ]
    print(LIST)

    a = json.dumps(LIST , sort_keys=True, indent=4, separators=(',', ': '))
    # a.replace("\", "")
    a.replace("\"", "")
    print(a)
    print(type(a))
    return HttpResponse(a,  content_type="application/json")

    # jsonfiles = json.dumps(a , indent=4)
    # return HttpResponse(jsonfiles,  content_type="application/json")



def Tech_Analysis(request):
    name = request.GET.get('name')
    enddate = date.today()
    TCS = get_history(symbol=name, start=date(2015,1,1), end=enddate)[['Open', 'High', 'Low', 'Close' , 'Volume']]
    print(TCS)
    TCS['t1'] = TCS['Close'].shift(-1)
    TCS['t2'] = TCS['t1'] - TCS['Close']
    TCS['Target'] = np.where(TCS['t2'] < 0, 0, 1)
    TCS.drop(['t2' , 't1'] , axis=1 , inplace=True)
    TCS.join(pd.get_dummies(TCS['Target']))
    print(TCS.head(10))
    df_trainY = TCS["Target"]
    df_trainX = TCS.drop("Target" , axis=1)
    x = df_trainX.values
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df_trainX = pd.DataFrame(x_scaled)
    X = df_trainX.iloc[:-1 :].values
    y = df_trainY.iloc[:-1].values
    XP =  df_trainX.iloc[-1: :].values
    n_comp = 20
    fastICA = FastICA(n_components = n_comp)
    fastICA.fit(X)
    X = fastICA.transform(X)
    clf1 = LogisticRegression(random_state=1)
    clf2 = XGBClassifier(random_state=1)
    clf3 = GaussianNB()
    model = VotingClassifier(estimators=[('lr', clf1), ('rf', clf2), ('gnb', clf3)], voting='soft')
    print(len(X))
    print(len(y))
    model.fit(X, y)
    y_pred = model.predict_proba(XP)
    a = "\nThere are {}% chances that the {} stock price will go up".format(y_pred[0][1]*100 , "TCS")

    return HttpResponse(a,  content_type="application/json")



def sectors(request):
    name = request.GET.get('name')
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    # query = "Select Distinct All_Stocks_Symbol_Data.Symbol, All_Stocks_Symbol_Data.Industry, All_Stocks_Symbol_Data.ISIN_No, All_Stocks_Symbol_Data.Status FROM All_Stocks_Symbol_Data INNER JOIN  Indian_Fundamentals_General_Ratios ON All_Stocks_Symbol_Data.Symbol = Indian_Fundamentals_General_Ratios.Company_Symbol where [Status]  LIKE '%Active%' AND ( Industry LIKE '%"+str(name) +"%')"

    query = "Select Distinct All_Stocks_Symbol_Data.Symbol, All_Stocks_Symbol_Data.Industry, All_Stocks_Symbol_Data.ISIN_No, All_Stocks_Symbol_Data.Status, Indian_Fundamentals_Financial_Statements.Abbreviation_Value FROM All_Stocks_Symbol_Data INNER JOIN  Indian_Fundamentals_Financial_Statements ON All_Stocks_Symbol_Data.Symbol = Indian_Fundamentals_Financial_Statements.Company_Symbol where [Status]  LIKE '%Active%' AND ( Industry LIKE '%"+str(name) +"%' ) AND Abbreviation='SREV' AND Fiscal_Year=2017 AND Fiscal_Period_Number=0"
    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)
    b['Abbreviation_Value'] = b['Abbreviation_Value'].astype(float).round(2)
    for i in b.index:
        b['Industry'][i] = b['Industry'][i].replace("amp;" , "")
        print(b['Industry'][i])
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def LR(request):
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "Select * FROM American_LinearRegression"
    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def IB_News(request):
    name = request.GET.get('name')
    ALL_LIST = []
    # def get_news(company_symbol, exchange='SMART', currency='USD'):
    asyncio.set_event_loop(asyncio.new_event_loop())
    ib = IB()
    ib.connect('www.finflock.com', 7777, clientId=102)
    company_symbol = name
    exchange = 'SMART'
    currency = 'USD'

    stock = Stock(company_symbol, exchange, currency)
    newsProviders = ib.reqNewsProviders()
    codes = '+'.join(np.code for np in newsProviders)
    ib.qualifyContracts(stock)
    end_date = datetime.date.today() - datetime.timedelta(days=1)
    start_date = end_date - datetime.timedelta(days=30)
    headlines = ib.reqHistoricalNews(stock.conId, codes, str(start_date), str(end_date), 100)
    print(start_date, end_date, len(headlines))
    headlines_mod = []
    for i in range(len(headlines)):
        _time = datetime.date(int(headlines[i].time.split(' ')[0].split('-')[0]), 
                            int(headlines[i].time.split(' ')[0].split('-')[1]), int(headlines[i].time.split(' ')[0].split('-')[2]))
        if _time <= end_date and _time >= start_date:
            headlines_mod.append(headlines[i])
    print(len(headlines_mod))
    articles = []
    for i in range(len(headlines_mod)):
        articles.append(ib.reqNewsArticle(headlines_mod[i].providerCode, headlines_mod[i].articleId).articleText)
    time = []
    headline = []
    for i in range(len(headlines_mod)):
        headline.append(headlines_mod[i].headline)
        time.append(headlines_mod[i].time)
    # ib.disconnect()
    ALL_LIST.append(time)
    ALL_LIST.append(headline)
    ALL_LIST.append(articles)
    print(ALL_LIST)
    b = pd.DataFrame({'Date': ALL_LIST[0] , 'Headline': ALL_LIST[1] , 'Article': ALL_LIST[2]})
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def news(request):
    from newsapi import NewsApiClient
    newsapi = NewsApiClient(api_key='592ad2ec918f40f39a479b7acdb2d416')
    top_headlines = newsapi.get_top_headlines(language='en' , sources='google-news')
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    name = request.GET.get('name')

    SQL_String = "News " + str(name)
    print("THIS IS SQL QUERY : " , SQL_String)

    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    print(hash)
    # r.delete(hash)
    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b = pickle.loads(r.get(hash))
        # print(b)

    else:
        print("LOADED FROM DB")
        from newsapi import NewsApiClient
        import pandas as pd
        from textblob import TextBlob
        newsapi = NewsApiClient(api_key='592ad2ec918f40f39a479b7acdb2d416')
        top_headlines = newsapi.get_top_headlines(language='en' , sources=name)
        LIST = []
        for i in range(0,len(top_headlines['articles'])-1):
            a = str(top_headlines['articles'][i]['description'])
            # a = "Good happy"
            blob = TextBlob(a)
            LIST.append([ top_headlines['articles'][i]['publishedAt'] , top_headlines['articles'][i]['title'] , top_headlines['articles'][i]['description'] , blob.sentiment.polarity ])
            print()
            
        b = pd.DataFrame( LIST )    
        b.columns = ['Date' , 'Headline' , 'Discription' , 'Sentiments']
        print(b)

        for m,i in enumerate(b.Sentiments):
            a = b.Sentiments[m]
            try:
                if (a < 0):
                    b.Sentiments[m] = "Negative"

                elif (a > 0):
                    b.Sentiments[m] = "Positive"

                else:
                    b.Sentiments[m] = "Neutral"
            except:
                pass



        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        r.expire(hash , 10*60)
        b  = pickle.loads(r.get(hash))

    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    data = b.to_json()
    return HttpResponse(jsonfiles,  content_type="application/json")



def fred(request):
    name = request.GET.get('name')
    quandl.ApiConfig.api_key = 'Yc9EU38DA1ry-YHirE6Y'
    quandl_code = "FRED/" + str(name)
    b = quandl.get(quandl_code)
    print(b.head())
    b['Date'] = b.index
    # b = b[['Date' , 'Value']]
    b.columns = ['Value' , 'Date']
    b.round(2)
    print(b.head())
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def fred2(request):
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    name = request.GET.get('name')
    quandl.ApiConfig.api_key = 'Yc9EU38DA1ry-YHirE6Y'
    quandl_code = "FRED/" + str(name)

    hash = hashlib.sha256(quandl_code.encode('utf-8')).hexdigest()
    print(hash)
    # r.delete(hash)
    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b  = pickle.loads(r.get(hash))
        b.round(2)
       

    else:
        print("LOADED FROM DB")
        b = quandl.get(quandl_code, start_date='2014-10-01', end_date='2018-10-01')
        # print(b.head())
        b['Date'] = b.index
        # b = b[['Date' , 'Value']]
        b.columns = ['Value' , 'Date']
        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        b  = pickle.loads(r.get(hash))
        b.round(2)
        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        r.expire(hash , 60*60*24)
        b  = pickle.loads(r.get(hash))
        # r.expire(hash , 100)
        # for i in range(0,b.shape[0]):
        #     b['Date'][i] = str(b['Date'][i]).split(" ")[0]
        # print(b.head())

        # b['TimeStamp'] = 0
        # # print(b.head())
        # for i in range(0, b.shape[0]):
        #     s = str(b['Date'][i]).split(" ")[0]
        #     # print(s)
        #     b['TimeStamp'][i] = time.mktime(datetime.datetime.strptime(s, "%Y-%d-%m").timetuple())
    LIST1 = []
    LIST2 = []
    
    for i in range(0,b.shape[0]):
        LIST1.append( str(b['Date'][i]).split(" ")[0] )
        LIST2.append( b['Value'][i])


    LIST = [{'Date' : LIST1} , {'Value' : LIST2} ,  {'Last_Price' : LIST2[-1]},  {'Change' : (LIST2[-1] - LIST2[-2])},  {'Per_Change' : ((LIST2[-1] - LIST2[-2])/ LIST2[-2])*100} ]


    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")




def screener(request):
    nse = Nse()

    name = request.GET.get('name')
    if name == 'gainers':
        b = nse.get_top_gainers()
    elif name == 'losers':
        b = nse.get_top_losers()

    b = pd.DataFrame(b)
    b['Change'] = ((b['highPrice'] - b['lowPrice'])/b['highPrice'])*100
    b['ChangeValue'] = ((b['highPrice'] - b['lowPrice']))
    b['Change'] = b['Change'].round(2)
    b = b[['symbol' , 'Change' , 'ChangeValue' , 'ltp']]
    

    b = b.to_json(orient='records')
    print(type(b))
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def signals(request):
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    name = request.GET.get('name')
    code = "Signal_" + str(name)
    hash = hashlib.sha256(code.encode('utf-8')).hexdigest()
    # r.delete(hash)

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b  = pickle.loads(r.get(hash))

    else:
        df = get_history(symbol=name, start=date(2018,9,1), end=date(2028,10,10))
        df['Timestamp'] = df.index
        df['Symbol'] = name
        df = df[['Timestamp' , 'Open' , 'High' , 'Low' , 'Close' , 'Volume']]
        df.reset_index(drop=True, inplace=True)
        df = add_all_ta_features(df, "Open", "High", "Low", "Close", "Volume", fillna=True)
        df['Symbol'] = name
        print(df.head())
        # df = df.head(5)
        print(df.shape[0])


        Indicators_List = ['trend_macd_signal', 'trend_macd_diff', 'trend_ema_fast','trend_ema_slow', 'trend_adx', 'trend_adx_pos', 'trend_adx_neg', 'trend_vortex_ind_pos', 'trend_vortex_ind_neg','trend_vortex_diff', 'trend_trix', 'trend_mass_index', 'trend_cci','trend_dpo', 'trend_kst', 'trend_kst_sig', 'trend_kst_diff','trend_ichimoku_a', 'trend_ichimoku_b', 'momentum_rsi', 'momentum_mfi','momentum_tsi', 'momentum_uo', 'momentum_stoch','momentum_stoch_signal', 'momentum_wr', 'momentum_ao']

        Signal_List = ['MACD_Signal', 'MACD_Diff_Signal', 'EMA_Fast_Signal','EMA_Slow_Signal', 'ADX_Signal', 'ADX_Pos_Signal', 'ADX_Neg_Signal', 'Vortex_Pos_Signal', 'Vortex_Neg_Signal','Vortex_Diff_Signal', 'Trix_Signal', 'Mass_Signal', 'CCI_Signal','DPO_Signal', 'KST_Signal', 'KST_Sig_Signal', 'KST_Diff_Signal','ICHIMOKU_Signal', 'ICHIMOKU_B_Signal', 'RSI_Signal', 'MFI_Signal','TSI_Signal', 'UO_Signal', 'Stoch_Signal','Stoch_Sig_Signal', 'WR_Signal', 'AO_Signal']

        for ind in range(0,len(Signal_List)):
            ## STRATEGY
            List1 = list(df[Indicators_List[ind]].nlargest(round(df.shape[0]*0.2)))
            List2 = list(df[Indicators_List[ind]].nsmallest(round(df.shape[0]*0.2)))
            print(List1)
            print(List2)

            df[Signal_List[ind]] = 0

            for i in range(0, df.shape[0]):
                if (df[Indicators_List[ind]][i] in List1):
                    # print("Sell")
                    df[Signal_List[ind]][i] = "Sell"
                elif (df[Indicators_List[ind]][i] in List2):
                    # print("Buy")
                    df[Signal_List[ind]][i] = "Buy"
                else :
                    # print("Hold")
                    df[Signal_List[ind]][i] = "Hold"

        # print(df.head())
        # df = df[["Symbol" , "Timestamp" , "Open", "High", "Low", "Close", "Volume" ,'MACD_Signal', 'MACD_Diff_Signal', 'EMA_Fast_Signal',
        #     'EMA_Slow_Signal', 'ADX_Signal', 'ADX_Pos_Signal', 'ADX_Neg_Signal',
        #     'ADX_Ind_Signal', 'Vortex_Pos_Signal', 'Vortex_Neg_Signal',
        #     'Vortex_Diff_Signal', 'Trix_Signal', 'Mass_Signal', 'CCI_Signal',
        #     'DPO_Signal', 'KST_Signal', 'KST_Sig_Signal', 'KST_Diff_Signal',
        #     'ICHIMOKU_Signal', 'ICHIMOKU_B_Signal', 'RSI_Signal', 'MFI_Signal',
        #     'TSI_Signal', 'UO_Signal', 'Stoch_Signal',
        #     'Stoch_Sig_Signal', 'WR_Signal', 'AO_Signal']]

        df = df[["Symbol" ,'Timestamp' , 'MACD_Signal', 'EMA_Fast_Signal',
                    'EMA_Slow_Signal', 'ADX_Signal',
                    'Vortex_Pos_Signal','Trix_Signal', 'Mass_Signal', 'CCI_Signal',
                    'DPO_Signal', 'KST_Signal',
                    'ICHIMOKU_Signal', 'RSI_Signal', 'MFI_Signal',
                    'TSI_Signal', 'UO_Signal', 'Stoch_Signal'
                    , 'WR_Signal', 'AO_Signal']]

        # df.columns = ['Symbol' ,'Timestamp', 'MACD', 'EMA',
        #     'EMAS', 'ADX',
        #     'Vortex','Trix', 'Mass', 'CCI',
        #     'DPO', 'KST',
        #     'ICHIMOKU', 'RSI', 'MFI',
        #     'TSI', 'UO',
        #     'Stoch', 'WR', 'AO']
        for i in range(0 , df.shape[0]):
            df['Timestamp'][i] = str(df['Timestamp'][i])


        b = df
        print(b)
        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        # b  = pickle.loads(r.get(hash))

    b = b.to_json(orient='records')
        # print(type(b))
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")




def signals_summary(request):
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    name = request.GET.get('name')
    code = "Signal_summary" + str(name)
    hash = hashlib.sha256(code.encode('utf-8')).hexdigest()
    # r.delete(hash)

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        LIST  = pickle.loads(r.get(hash))

    else:
        df = get_history(symbol=name, start=date(2018,9,1), end=date(2028,10,10))
        df['Timestamp'] = df.index
        df['Symbol'] = name
        df = df[['Timestamp' , 'Open' , 'High' , 'Low' , 'Close' , 'Volume']]
        df.reset_index(drop=True, inplace=True)
        df = add_all_ta_features(df, "Open", "High", "Low", "Close", "Volume", fillna=True)
        df['Symbol'] = name
        print(df.head())


        Indicators_List = ['trend_macd_signal', 'trend_macd_diff', 'trend_ema_fast','trend_ema_slow', 'trend_adx', 'trend_adx_pos', 'trend_adx_neg', 'trend_vortex_ind_pos', 'trend_vortex_ind_neg','trend_vortex_diff', 'trend_trix', 'trend_mass_index', 'trend_cci','trend_dpo', 'trend_kst', 'trend_kst_sig', 'trend_kst_diff','trend_ichimoku_a', 'trend_ichimoku_b', 'momentum_rsi', 'momentum_mfi','momentum_tsi', 'momentum_uo', 'momentum_stoch','momentum_stoch_signal', 'momentum_wr', 'momentum_ao']

        Signal_List = ['MACD_Signal', 'MACD_Diff_Signal', 'EMA_Fast_Signal','EMA_Slow_Signal', 'ADX_Signal', 'ADX_Pos_Signal', 'ADX_Neg_Signal', 'Vortex_Pos_Signal', 'Vortex_Neg_Signal','Vortex_Diff_Signal', 'Trix_Signal', 'Mass_Signal', 'CCI_Signal','DPO_Signal', 'KST_Signal', 'KST_Sig_Signal', 'KST_Diff_Signal','ICHIMOKU_Signal', 'ICHIMOKU_B_Signal', 'RSI_Signal', 'MFI_Signal','TSI_Signal', 'UO_Signal', 'Stoch_Signal','Stoch_Sig_Signal', 'WR_Signal', 'AO_Signal']

        for ind in range(0,len(Signal_List)):
            ## STRATEGY
            List1 = list(df[Indicators_List[ind]].nlargest(round(df.shape[0]*0.2)))
            List2 = list(df[Indicators_List[ind]].nsmallest(round(df.shape[0]*0.2)))

            df[Signal_List[ind]] = 0

            for i in range(0, df.shape[0]):
                if (df[Indicators_List[ind]][i] in List1):
                    df[Signal_List[ind]][i] = "Sell"
                elif (df[Indicators_List[ind]][i] in List2):
                    df[Signal_List[ind]][i] = "Buy"
                else :
                    df[Signal_List[ind]][i] = "Hold"


        df = df[["Symbol" ,'Timestamp' , 'MACD_Signal', 'EMA_Fast_Signal',
                    'EMA_Slow_Signal', 'ADX_Signal',
                    'Vortex_Pos_Signal','Trix_Signal', 'Mass_Signal', 'CCI_Signal',
                    'DPO_Signal', 'KST_Signal',
                    'ICHIMOKU_Signal', 'RSI_Signal', 'MFI_Signal',
                    'TSI_Signal', 'UO_Signal', 'Stoch_Signal'
                    , 'WR_Signal', 'AO_Signal']]

        df.columns = ['Symbol' ,'Timestamp', 'MACD', 'EMA',
            'EMAS', 'ADX',
            'Vortex','Trix', 'Mass', 'CCI',
            'DPO', 'KST',
            'ICHIMOKU', 'RSI', 'MFI',
            'TSI', 'UO',
            'Stoch', 'WR', 'AO']
        for i in range(0 , df.shape[0]):
            df['Timestamp'][i] = str(df['Timestamp'][i])


        b = df
        b = b.tail(1)
        
        print(b)
        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)

        blist = b.values.tolist()
        print("This is BList" , blist)
        bb1 = blist[0].count("Buy")
        bb2 = blist[0].count("Sell")
        bb3 = blist[0].count("Hold")
        if (bb1%2 == 0):
            o1 = bb1/2
            o2 = bb1/2
        else:
            o1 = bb1//2
            o2 = bb1//2 + 1

        if (bb2%2 == 0):
            m1 = bb2/2
            m2 = bb2/2
        else:
            m1 = bb2//2
            m2 = bb2//2 + 1

        if (bb3%2 == 0):
            h1 = bb3/2
            h2 = bb3/2
        else:
            h1 = bb3//2
            h2 = bb3//2 + 1

        LIST = [ {'O_Buy' : o1} , {'O_Sell' : m1} , {'O_Hold' : h1} , {'MA_Buy' : o2} , {'MA_Sell' : m2} , {'MA_Hold' : h2} , {'S_Buy' : bb1} , {'S_Sell' : bb2} , {'S_Hold' : bb3} ]

        pickobj = pickle.dumps(LIST)
        r.set(hash , pickobj)
        r.expire(hash , 60*60*24)
        LIST  = pickle.loads(r.get(hash))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def signals_all(request):
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    code_signals_all = "Signal_All"
    hash_signals_all = hashlib.sha256(code_signals_all.encode('utf-8')).hexdigest()
    # r.delete(hash)

    if(r.get(hash_signals_all)):
        print("LOADED FROM CACHE")
        b  = pickle.loads(r.get(hash_signals_all))

    else:
        
        cnx = sqlite3.connect('Daily_Data.db')
        b = pd.read_sql('select * from Indian_Technicals', con=cnx)
    b.drop_duplicates(subset=['Symbol'] , inplace=True)

    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")




def top_bar(request):

    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "topbar"
    hash_topbar = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash_topbar)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash_topbar))

    else:
        print("LOADED FROM DB")
        
        df = pd.DataFrame()
        quandl.ApiConfig.api_key = 'Yc9EU38DA1ry-YHirE6Y'

        df = quandl.get('PERTH/AUD_USD_D')
        AUD_USD_D = df.tail(1)['Bid High'][0]
        AUD_USD_D_Change = round(df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0] , 3)
        AUD_USD_D_PChange = round(((df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0])/df.tail(1)['Bid High'][0])*100 , 2)

        df = quandl.get('PERTH/GOLD_USD_D')
        GOLD_USD_D = df.tail(1)['Bid High'][0]
        GOLD_USD_D_Change = round(df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0] , 3)
        GOLD_USD_D_PChange = round(((df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0])/df.tail(1)['Bid High'][0])*100 , 2)

        df = quandl.get('PERTH/SLVR_USD_D')
        SLVR_USD_D = df.tail(1)['Bid High'][0]
        SLVR_USD_D_Change = round(df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0] , 3)
        SLVR_USD_D_PChange = round(((df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0])/df.tail(1)['Bid High'][0])*100 , 2)

        df = quandl.get('PERTH/USD_JPY_D')
        USD_JPY_D = df.tail(1)['Bid High'][0]
        USD_JPY_D_Change = round(df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0] , 3)
        USD_JPY_D_PChange = round(((df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0])/df.tail(1)['Bid High'][0])*100 , 2)


        df = quandl.get('NASDAQOMX/NQUSB')
        NASDAQ = df.tail(1)['Index Value'][0]
        NASDAQ_Change = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0]) ) , 3)
        NASDAQ_PChange = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0])/df.tail(1)['Index Value'][0] )*100 , 2)


        df = quandl.get('NASDAQOMX/NQGI')
        df = df.fillna(0)
        NQGI = df.tail(1)['Index Value'][0]
        NQGI_Change = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0]) ) , 3)
        NQGI_PChange = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0])/df.tail(1)['Index Value'][0] )*100 , 2)

        df = quandl.get('NASDAQOMX/NQASIA')
        NQASIA = df.tail(1)['Index Value'][0]
        NQASIA_Change = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0]) ) , 3)
        NQASIA_PChange = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0])/df.tail(1)['Index Value'][0] )*100 , 2)

        df = quandl.get('NASDAQOMX/NQIN')
        NQIN = df.tail(1)['Index Value'][0]
        NQIN_Change = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0]) ) , 3)
        NQIN_PChange = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0])/df.tail(1)['Index Value'][0] )*100 , 2)


        LIST = [ {'AUD_USD_D' : AUD_USD_D} , {'AUD_USD_D_Change' : AUD_USD_D_Change}  , {'AUD_USD_D_PChange' : AUD_USD_D_PChange}   , {'GOLD_USD_D' : GOLD_USD_D} , {'GOLD_USD_D_Change' : GOLD_USD_D_Change}  , {'GOLD_USD_D_PChange' : GOLD_USD_D_PChange} , {'SLVR_USD_D' : SLVR_USD_D} , {'SLVR_USD_D_Change' : SLVR_USD_D_Change}  , {'SLVR_USD_D_PChange' : SLVR_USD_D_PChange} , {'USD_JPY_D' : USD_JPY_D} , {'USD_JPY_D_Change' : USD_JPY_D_Change}  , {'USD_JPY_D_PChange' : USD_JPY_D_PChange} , {'NASDAQ' : NASDAQ} , {'NASDAQ_Change' : NASDAQ_Change}  , {'NASDAQ_PChange' : NASDAQ_PChange}  , {'NQGI' : NQGI} , {'NQGI_Change' : NQGI_Change}  , {'NQGI_PChange' : NQGI_PChange}   , {'NQASIA' : NQASIA} , {'NQASIA_Change' : NQASIA_Change}  , {'NQASIA_PChange' : NQASIA_PChange}  , {'NQIN' : NQIN} , {'NQIN_Change' : NQIN_Change}  , {'NQIN_PChange' : NQIN_PChange} ]

        pickobj = pickle.dumps(LIST)
        r.set(hash_topbar , pickobj)
        r.expire(hash_topbar , 60*60*24)
        LIST  = pickle.loads(r.get(hash_topbar))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")






def sbox1(request):

    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "sbox1"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        
        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=JPY&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        USD_JPY = df_list.iloc[4]['Realtime Currency Exchange Rate']
        print(USD_JPY)

        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=INR&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        USD_INR = df_list.iloc[4]['Realtime Currency Exchange Rate']
        print(USD_INR)

        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=AUD&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        USD_AUD = df_list.iloc[4]['Realtime Currency Exchange Rate']
        print(USD_AUD)

        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=EUR&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        USD_EUR = df_list.iloc[4]['Realtime Currency Exchange Rate']
        print(USD_EUR)

        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=GBP&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        USD_GBP = df_list.iloc[4]['Realtime Currency Exchange Rate']
        print(USD_GBP)


        LIST = [ {'USD_JPY' : USD_JPY} , {'USD_INR' : USD_INR} , {'USD_AUD' : USD_AUD} , {'USD_EUR' : USD_EUR} , {'USD_GBP' : USD_GBP} ]

        pickobj = pickle.dumps(LIST)
        r.set(hash , pickobj)
        r.expire(hash , 60*60*24)
        LIST  = pickle.loads(r.get(hash))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")




def sector_overall(request):

    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "sbox2"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")


        url = 'https://www.alphavantage.co/query?function=SECTOR&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        df = df_list['Rank A: Real-Time Performance']
        print(df)
        Communication = df[0]
        ConsumerDiscretionary = df[1]
        ConsumerStaples = df[2]
        Energy = df[3]
        Financials = df[4]
        HealthCare = df[5]
        Industrials = df[6]
        InformationTechnology = df[8]
        Materials = df[10]
        RealEstate = df[11]
        Utilities = df[12]
        

        LIST = {'CommunicationServices' : Communication , 'ConsumerStaples' : ConsumerStaples ,'Energy' : Energy ,'Financials' : Financials , 'ConsumerDiscretionary' : ConsumerDiscretionary , 'HealthCare' : HealthCare  , 'Industrials' : Industrials  , 'InformationTechnology' : InformationTechnology , 'Materials' : Materials, 'RealEstate' : RealEstate, 'Utilities' : Utilities }


        # LIST = [ {'Financials' : Financials}  , {'HealthCare' : HealthCare}  , {'Industrials' : Industrials}  , {'InformationTechnology' : InformationTechnology}  , {'Communication' : Communication}]

        pickobj = pickle.dumps(LIST)
        r.set(hash , pickobj)
        r.expire(hash , 60*60*24)
        LIST  = pickle.loads(r.get(hash))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def sbox3(request):

    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "sbox3"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        
        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTC&to_currency=CNY&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        BTC = df_list.iloc[4]['Realtime Currency Exchange Rate']
        print(BTC)


        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=ETH&to_currency=CNY&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        ETH = df_list.iloc[4]['Realtime Currency Exchange Rate']
        print(ETH)


        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=ETC&to_currency=GBP&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        ETC = df_list.iloc[4]['Realtime Currency Exchange Rate']
        print(ETC)


        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XMR&to_currency=GBP&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        XMR = df_list.iloc[4]['Realtime Currency Exchange Rate']
        print(XMR)


        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTG&to_currency=GBP&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        BTG = df_list.iloc[4]['Realtime Currency Exchange Rate']
        print(BTG)


        LIST = [{'BTC' : BTC} , {'ETH' : ETH} , {'ETC' : ETC} , {'XMR' : XMR} , {'BTG' : BTG}]

        pickobj = pickle.dumps(LIST)
        r.set(hash , pickobj)
        r.expire(hash , 60*60*24)
        LIST  = pickle.loads(r.get(hash))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")




def Mutual_Funds(request):

    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "lbox1"
    hash_Mutual_Funds = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash_Mutual_Funds)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash_Mutual_Funds))

    else:
        print("LOADED FROM DB")


        df = quandl.get('AMFI/143351')
        IDBI = df.tail(1)['Net Asset Value'][0]
        IDBI_Change = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])) , 3)
        IDBI_PChange = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])/df.tail(1)['Net Asset Value'][0] )*100 , 2)


        df = quandl.get('AMFI/113076')
        ICICI = df.tail(1)['Net Asset Value'][0]
        ICICI_Change = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])) , 3)
        ICICI_PChange = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])/df.tail(1)['Net Asset Value'][0] )*100 , 2)


        df = quandl.get('AMFI/126963')
        TATA = df.tail(1)['Net Asset Value'][0]
        TATA_Change = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])) , 3)
        TATA_PChange = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])/df.tail(1)['Net Asset Value'][0] )*100 , 2)


        df = quandl.get('AMFI/100033')
        Aditya_Birla = df.tail(1)['Net Asset Value'][0]
        Aditya_Birla_Change = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0]) ) , 3)
        Aditya_Birla_PChange = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])/df.tail(1)['Net Asset Value'][0] )*100 , 2)


        df = quandl.get('AMFI/100868')
        HDFC = df.tail(1)['Net Asset Value'][0]
        HDFC_Change = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])) , 3)
        HDFC_PChange = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])/df.tail(1)['Net Asset Value'][0] )*100 , 2)


        LIST = [{'IDBI' : IDBI} , {'IDBI_Change' : IDBI_Change}  , {'IDBI_PChange' : IDBI_PChange}  , {'ICICI' : ICICI} , {'ICICI_Change' : ICICI_Change}  , {'ICICI_PChange' : ICICI_PChange}  , {'TATA' : TATA} , {'TATA_Change' : TATA_Change}  , {'TATA_PChange' : TATA_PChange}   , {'Aditya_Birla' : Aditya_Birla} , {'Aditya_Birla_Change' : Aditya_Birla_Change}  , {'Aditya_Birla_PChange' : Aditya_Birla_PChange}  , {'HDFC' : HDFC} , {'HDFC_Change' : HDFC_Change}  , {'HDFC_PChange' : HDFC_PChange} ]



        pickobj = pickle.dumps(LIST)
        r.set(hash_Mutual_Funds , pickobj)
        r.expire(hash_Mutual_Funds , 60*60*24)
        LIST  = pickle.loads(r.get(hash_Mutual_Funds))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def Futures(request):

    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "lbox2"
    hash_Futures = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash_Futures)
    # r.delete(hash_Futures)


    if(r.get(hash_Futures)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash_Futures))

    else:
        print("LOADED FROM DB")


        df = quandl.get('MCX/GMU2018')
        IDBI = df.tail(1)['High'][0]
        IDBI_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        IDBI_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('MCX/ALMX2015')
        ICICI = df.tail(1)['High'][0]
        ICICI_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        ICICI_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('MCX/SIMJ2017')
        TATA = df.tail(1)['High'][0]
        TATA_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        TATA_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('MCX/PBMQ2017')
        Aditya_Birla = df.tail(1)['High'][0]
        Aditya_Birla_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        Aditya_Birla_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('MCX/CUMV2017')
        HDFC = df.tail(1)['High'][0]
        HDFC_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        HDFC_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        LIST = [{'Gold_Mini' : IDBI} , {'Gold_Mini_Change' : IDBI_Change}  , {'Gold_Mini_PChange' : IDBI_PChange}  , {'Aluminium_Mini' : ICICI} , {'Aluminium_Mini_Change' : ICICI_Change}  , {'Aluminium_Mini_PChange' : ICICI_PChange}  , {'Silver_Mini' : TATA} , {'Silver_Mini_Change' : TATA_Change}  , {'Silver_Mini_PChange' : TATA_PChange}   , {'Lead_Mini' : Aditya_Birla} , {'Lead_Mini_Change' : Aditya_Birla_Change}  , {'Lead_Mini_PChange' : Aditya_Birla_PChange}  , {'Copper_Mini' : HDFC} , {'Copper_Mini_Change' : HDFC_Change}  , {'Copper_Mini_PChange' : HDFC_PChange} ]

        pickobj = pickle.dumps(LIST)
        r.set(hash_Futures , pickobj)
        r.expire(hash_Futures , 60*60*24)
        LIST  = pickle.loads(r.get(hash_Futures))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def ETF(request):

    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "lbox3"
    hash_ETF = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash_ETF)
    # r.delete(hash_ETF)


    if(r.get(hash_ETF)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash_ETF))

    else:
        print("LOADED FROM DB")


        df = quandl.get('NSE/SETFGOLD')
        IDBI = df.tail(1)['High'][0]
        IDBI_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        IDBI_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('NSE/KOTAKNV20')
        ICICI = df.tail(1)['High'][0]
        ICICI_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])) , 3)
        ICICI_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('NSE/SETFNIF50')
        TATA = df.tail(1)['High'][0]
        TATA_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])) , 3)
        TATA_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('NSE/ICICISENSX')
        Aditya_Birla = df.tail(1)['High'][0]
        Aditya_Birla_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        Aditya_Birla_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('NSE/AXISGOLD')
        HDFC = df.tail(1)['High'][0]
        HDFC_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        HDFC_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        LIST = [{'SBI_Gold' : IDBI} , {'SBI_Gold_Change' : IDBI_Change}  , {'SBI_Gold_PChange' : IDBI_PChange}  , {'Kotak_Mamc' : ICICI} , {'Kotak_MamcChange' : ICICI_Change}  , {'Kotak_Mamc_PChange' : ICICI_PChange}  , {'SBI_Nifty50' : TATA} , {'SBI_Nifty50_Change' : TATA_Change}  , {'SBI_Nifty50_PChange' : TATA_PChange}   , {'ICICI_Sensex' : Aditya_Birla} , {'ICICI_Sensex_Change' : Aditya_Birla_Change}  , {'ICICI_Sensex_PChange' : Aditya_Birla_PChange}  , {'Axis_Gold' : HDFC} , {'Axis_Gold_Change' : HDFC_Change}  , {'Axis_Gold_PChange' : HDFC_PChange} ]

        pickobj = pickle.dumps(LIST)
        r.set(hash_ETF , pickobj)
        r.expire(hash_ETF , 60*60*24)
        LIST  = pickle.loads(r.get(hash_ETF))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def Exchange_rates(request):

    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "lbox4"
    hash_Exchange_rates = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash_Exchange_rates)
    # r.delete(hash_Exchange_rates)


    if(r.get(hash_Exchange_rates)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash_Exchange_rates))

    else:
        print("LOADED FROM DB")


        df = quandl.get('BUNDESBANK/BBEX3_M_AUD_USD_CM_AC_A01')
        IDBI = df.tail(1)['Value'][0]
        IDBI_Change = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0]) ) , 3)
        IDBI_PChange = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0])/df.tail(1)['Value'][0] )*100 , 2)


        df = quandl.get('BUNDESBANK/BBEX3_M_JPY_USD_CA_AA_A02')
        ICICI = df.tail(1)['Value'][0]
        ICICI_Change = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0])) , 3)
        ICICI_PChange = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0])/df.tail(1)['Value'][0] )*100 , 2)


        df = quandl.get('BUNDESBANK/BBEX3_M_CNY_USD_CA_AC_A01')
        TATA = df.tail(1)['Value'][0]
        TATA_Change = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0]) ) , 3)
        TATA_PChange = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0])/df.tail(1)['Value'][0] )*100 , 2)


        df = quandl.get('BUNDESBANK/BBEX3_M_INR_USD_CA_AC_A01')
        Aditya_Birla = df.tail(1)['Value'][0]
        Aditya_Birla_Change = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0]) ) , 3)
        Aditya_Birla_PChange = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0])/df.tail(1)['Value'][0] )*100 , 2)


        df = quandl.get('BUNDESBANK/BBEX3_M_USD_EUR_BB_AC_A02')
        HDFC = df.tail(1)['Value'][0]
        HDFC_Change = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0]) ) , 3)
        HDFC_PChange = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0])/df.tail(1)['Value'][0] )*100 , 2)


        LIST = [{'USD_AUD' : IDBI} , {'USD_AUD_Change' : IDBI_Change}  , {'USD_AUD_PChange' : IDBI_PChange}  , {'USD_JPY' : ICICI} , {'USD_JPY_Change' : ICICI_Change}  , {'USD_JPY_PChange' : ICICI_PChange}  , {'USD_CNY' : TATA} , {'USD_CNY_Change' : TATA_Change}  , {'USD_CNY_PChange' : TATA_PChange}   , {'USD_INR' : Aditya_Birla} , {'USD_INR_Change' : Aditya_Birla_Change}  , {'USD_INR_PChange' : Aditya_Birla_PChange}  , {'USD_EUR' : HDFC} , {'USD_EUR_Change' : HDFC_Change}  , {'USD_EUR_PChange' : HDFC_PChange} ]


        pickobj = pickle.dumps(LIST)
        r.set(hash_Exchange_rates , pickobj)
        r.expire(hash_Exchange_rates , 60*60*24)
        LIST  = pickle.loads(r.get(hash_Exchange_rates))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def Mutual_Fundsgraph(request):
    name = request.GET.get('name')
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "lbox1graph" + str(name)
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)
    List = []

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        dictt = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        b = quandl.get(name)
        b['Date'] = b.index
        print(b.columns)
        b = b[['Date' , 'Net Asset Value']]
        b['Date'] = b.index
        b['ts'] = b.Date.values.astype(np.int64)
        b['ts'] = b['ts']//1000000

        for index, row in b.iterrows():
            List.append([ row['ts'] , row['Net Asset Value'] ])
        dictt  = {'data' : List}

        pickobj = pickle.dumps(dictt)
        r.set(hash , pickobj)
        r.expire(hash , 60*60*24)
        dictt  = pickle.loads(r.get(hash))


    a = json.dumps(dictt  , indent=4)
    return HttpResponse(a,  content_type="application/json")



def Futuresgraph(request):
    name = request.GET.get('name')
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "lbox2graph" + str(name)
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)
    List = []

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        dictt = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        b = quandl.get(name)
        b['Date'] = b.index
        b = b[['Date' , 'Close']]
        b['Date'] = b.index
        b['ts'] = b.Date.values.astype(np.int64)
        b['ts'] = b['ts']//1000000

        for index, row in b.iterrows():
            List.append([ row['ts'] , row['Close'] ])
        dictt  = {'data' : List}

        pickobj = pickle.dumps(dictt)
        r.set(hash , pickobj)
        r.expire(hash , 60*60*24)
        dictt  = pickle.loads(r.get(hash))


    a = json.dumps(dictt  , indent=4)
    return HttpResponse(a,  content_type="application/json")



def ETFgraph(request):
    name = request.GET.get('name')
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "lbox3graph" + str(name)
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)
    List = []

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        dictt = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        b = quandl.get(name)
        b['Date'] = b.index
        b = b[['Date' , 'Close']]
        b['Date'] = b.index
        b['ts'] = b.Date.values.astype(np.int64)
        b['ts'] = b['ts']//1000000

        for index, row in b.iterrows():
            List.append([ row['ts'] , row['Close'] ])
        dictt  = {'data' : List}

        pickobj = pickle.dumps(dictt)
        r.set(hash , pickobj)
        r.expire(hash , 60*60*24)
        dictt = pickle.loads(r.get(hash))


    a = json.dumps(dictt  , indent=4)
    return HttpResponse(a,  content_type="application/json")



def Exchange_ratesgraph(request):
    name = request.GET.get('name')
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "lbox4graph" + str(name)
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)
    List = []

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        dictt = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        b = quandl.get(name)
        b['Date'] = b.index
        print(b.head())
        b = b[['Date' , 'Value']]
        b['Date'] = b.index
        b['ts'] = b.Date.values.astype(np.int64)
        b['ts'] = b['ts']//1000000

        for index, row in b.iterrows():
            List.append([ row['ts'] , row['Value'] ])
        dictt  = {'data' : List}

        pickobj = pickle.dumps(dictt)
        r.set(hash , pickobj)
        r.expire(hash , 60*60*24)
        dictt  = pickle.loads(r.get(hash))


    a = json.dumps(dictt  , indent=4)
    return HttpResponse(a,  content_type="application/json")




def quandl_universe(request):
    name = request.GET.get('name')
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "quandl" + str(name)
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    r.delete(hash)
    List = []

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        b = quandl.get(name)
        b['Date'] = b.index
        b = b[['Date' , 'Value']]
        b['Date'] = b.index
        b['ts'] = b.Date.values.astype(np.int64)
        b['ts'] = b['ts']//1000000

        for index, row in b.iterrows():
            List.append([ row['ts'] , row['Value'] ])
        dictt  = {'data' : List}

        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        r.expire(hash , 60*60*24)
        b  = pickle.loads(r.get(hash))


    a = json.dumps(dictt  , indent=4)
    return HttpResponse(a,  content_type="application/json")


ALL_DF_FS = pd.read_csv('American_Financial_Statements.csv')

def LR_Compare(request):
    name = request.GET.get('name')
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "LR_Compare_" + str(name)
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b = pickle.loads(r.get(hash))
        b.round(2)

    else:
        print("LOADED FROM DB")

        cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
        cursor = cnxn.cursor()
        query = "select Symbol from American_Stocks_Sectors"
        a = pd.read_sql_query(query , cnxn)
        a = list(a['Symbol'])
        a = a[0:50]

        a = ['AAPL' , 'IBM' , 'GOOG' , 'MSFT' , 'ABB' , 'ANF' , 'ACCO' , 'ADNT' , 'AFL']
        
        b = 'American'
        c = name
        x = []

        Final_DF = pd.DataFrame()

        for i in a:
            try:
                # ALL_DF_FS.loc[(ALL_DF_FS['Company_Symbol'] == i) & (ALL_DF_FS['Abbreviation'] == c)& (ALL_DF_FS['Fiscal_Period_Type'] == 'Annual')]
                # query = "http://www.finflock.com/api/?select=End_Date,Abbreviation_Value&fro="+ b +"_Fundamentals_Financial_Statements&where=Company_Symbol='"+ i +"' AND Abbreviation='"+ c +"' AND Fiscal_Period_Type='Annual'"
                # print(query)
                # html = requests.get(query).content
                # df = pd.read_json(html)
                df = ALL_DF_FS.loc[(ALL_DF_FS['Company_Symbol'] == i) & (ALL_DF_FS['Abbreviation'] == c)& (ALL_DF_FS['Fiscal_Period_Type'] == 'Annual')]
                df = df.sort_values(['End_Date'], ascending=True)
        
        
                
                Final_DF[i] =  list(df['Abbreviation_Value'])
                
        #        Final_DF['Date'] = df['End_Date']

                x.append(i)
            except:
                pass
            
        print(Final_DF)
        try:
            Final_DF['Date'] = list(df['End_Date'])
        except:
            pass
        print(Final_DF)  
        columns = ['Date']
        columns.extend(x)

        Final_DF = Final_DF[columns]
        Final_DF.index = Final_DF['Date']
        Final_DF.drop('Date' , axis=1 , inplace=True)
        b = Final_DF.T
        b['Symbol'] = b.index
        print(b)



        cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
        cursor = cnxn.cursor()
        query = "select * from American_Stocks_Sectors"
        s = pd.read_sql_query(query , cnxn)
        c = pd.merge(b, s, how='inner')
        c['Prediction'] = 0

        for i in range( 0 , c.shape[0]):
            try:
                model = LinearRegression()
                model.fit(pd.DataFrame([1,2,3,4,5,6]), pd.DataFrame([c.iloc[i][0] , c.iloc[i][1] , c.iloc[i][2] , c.iloc[i][3] , c.iloc[i][4] , c.iloc[i][5] ]))
                c['Prediction'][i] = model.predict(7)[0][0]
            except:
                c['Prediction'][i] = 'NAN'

        b = c
        b.columns = ['Dec 2012' , 'Dec 2013' , 'Dec 2014' , 'Dec 2015' , 'Dec 2016' , 'Dec 2017' , 'Symbol' , 'Name' , 'MarketCap' , 'Sector' , 'Prediction']


        # b['Dec 2012'] = float(b['Dec 2012']).round(2)
        # b['Dec 2013'] = float(b['Dec 2013']).round(2)
        # b['Dec 2014'] = float(b['Dec 2014']).round(2)
        # b['Dec 2015'] = float(b['Dec 2015']).round(2)
        # b['Dec 2016'] = float(b['Dec 2016']).round(2)
        # b['Dec 2017'] = float(b['Dec 2017']).round(2)
        b['Signal'] = 0
        for i in range( 0 , b.shape[0]):
            try:
                if b['Prediction'][i] - float(b['Dec 2017'][i])  > 0:
                    print("BUY")
                    b['Signal'][i] = "BUY"
                else:
                    print("SELL")
                    b['Signal'][i] = "SELL"
            except:
                b['Signal'][i] = "NAN"





        b.drop(b.index[0] , inplace=True)
        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        # r.expire(hash , 60*60*24)
        b  = pickle.loads(r.get(hash))

    b['SparkLine'] = 0

    Temp_List = []
    for m,i in enumerate(b.iterrows()):
        try:
            print(i)
            Temp_List.append([i[1]['Dec 2012'] , i[1]['Dec 2013'] , i[1]['Dec 2014'] , i[1]['Dec 2015'] , i[1]['Dec 2016'] , i[1]['Dec 2017']])
            xx = str(Temp_List)
            print(xx)
            b.at[m,'SparkLine'] = xx
            
            Temp_List = []
        except Exception as e:
            print(e)

    b.drop_duplicates(subset=['Symbol'] , inplace=True)
    print(b)
    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def Scanner(request):
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "Scanner_Stockmaniacs"
    hash_Scanner = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash_Scanner)):
        print("LOADED FROM CACHE")
        b = pickle.loads(r.get(hash_Scanner))

    else:
        print("LOADED FROM DB")

        query = "https://www.stockmaniacs.net/freebies/nse-stock-screener/"
        html = requests.get(query).content
        df = pd.read_html(html)
        b = df[0]
        b = pd.DataFrame(b)
        b.drop(['GAP >1%' , 'OPEN STATUS'] , axis=1 , inplace=True)
        b.columns = ['Symbol' , 'Open' , 'High' , 'Low' , 'LTP' , 'Volume' , 'Per_Change' , 'Last_Close' , 'Short_Trend' , 'Long_Trend']

        pickobj = pickle.dumps(b)
        r.set(hash_Scanner , pickobj)
        r.expire(hash_Scanner , 60*60*24)
        b  = pickle.loads(r.get(hash_Scanner))

    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def Sector(request):
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "Sector"
    hash_Sector = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash_Sector)
    # r.delete(hash_Sector)


    if(r.get(hash_Sector)):
        print("LOADED FROM CACHE")
        b = pickle.loads(r.get(hash_Sector))

    else:
        print("LOADED FROM DB")

        cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
        cursor = cnxn.cursor()
        query = "select * from American_Stocks_Sectors"
        b = pd.read_sql_query(query , cnxn)
        b.drop(b.index[0] , inplace=True)

        pickobj = pickle.dumps(b)
        r.set(hash_Sector , pickobj)
        r.expire(hash_Sector , 60*60*24)
        b  = pickle.loads(r.get(hash_Sector))
        

    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def Sector_I(request):
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    SQL_String = "Sector_I"
    hash_SQL_String = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash_SQL_String)
    # r.delete(hash_SQL_String)


    if(r.get(hash_SQL_String)):
        print("LOADED FROM CACHE")
        b = pickle.loads(r.get(hash_SQL_String))

    else:
        print("LOADED FROM DB")

        sheets = ['Automobile',
        'Banking&Finance',
        'Construction&Cements',
        'Chemicals',
        'Conglomerates',
        'Cons Durable',
        'Cons Non Durable',
        'Engineering',
        'Food & beverages',
        'GOLD ETF',
        'Manufacuring',
        'Media',
        'Metals&Mining',
        'Miscellaneous', 
        'Oil & Gas',
        'Pharma',
        'Retail & Real Estate',
        'Services',
        'Telecom',
        'Tobacco',
        'Technology',
        'utilities']

        Final_Df = pd.DataFrame()

        for i in sheets:
            df = pd.read_excel('nse sector wise.xlsx' , sheetname=i)
            df['Sector'] = i
            print(df.shape)
            Final_Df = Final_Df.append(df)
            b = Final_Df[['Company Name' , 'Mkt Cap' , 'Sector' , 'Industry']]
            b.drop(b.index[0] , inplace=True)

        pickobj = pickle.dumps(b)
        r.set(hash_SQL_String , pickobj)
        r.expire(hash_SQL_String , 60*60*24)
        b  = pickle.loads(r.get(hash_SQL_String))

    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



# def Login(request):
#     username = request.GET.get('username')
#     password = request.GET.get('password')

#     cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
#     cursor = cnxn.cursor()
#     query = "select * from R_Login3"

#     b = pd.read_sql_query(query , cnxn)
#     b = pd.DataFrame(b)

#     if ( (b.loc[(b['Username'] == username) & (b['PasswordHash'] == password)]).shape[0] > 0 ):
#         result = "True"
#     else:
#         result = "False"


#     return HttpResponse(result,  content_type="application/json")



# def Login(request):
#     username = request.GET.get('username')

#     cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
#     cursor = cnxn.cursor()
#     query = "select * from Dashboard_Login"

#     b = pd.read_sql_query(query , cnxn)
#     b = pd.DataFrame(b)

#     b = b.loc[ (b['Username'] == username)].tail(1)
#     LIST = [ {'Name' : b['Name'].iloc[0]} , {'Password' : b['PasswordHash'].iloc[0]} , {'Username' : b['Username'].iloc[0]} , {'Email' : b['Email'].iloc[0]}, {'CSS_Color' : b['CSS_Color'].iloc[0]} ]
#     print(LIST)
#     jsonfiles = json.dumps(LIST  , indent=4)
#     return HttpResponse(jsonfiles,  content_type="application/json")

def Login(request):
    username = request.GET.get('username')

    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "select * from Dashboard_Login"

    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)

    b = b.loc[ (b['Username'] == username)].tail(1)
    LIST = [ {'Name' : b['Name'].iloc[0]} , {'Password' : b['PasswordHash'].iloc[0]} , {'Username' : b['Username'].iloc[0]} , {'Email' : b['Email'].iloc[0]}]
    print(LIST)
    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


# def Register(request):
#     name = request.GET.get('name')
#     email = request.GET.get('email')
#     username = request.GET.get('username')
#     password = request.GET.get('password')


#     cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
#     cursor = cnxn.cursor()
#     cursor.execute("INSERT INTO R_Login3(Name , Email, Username , PasswordHash )" "VALUES (?, ?, ?, ?)", (name , email, username , password))
#     cnxn.commit()

#     query = "select * from R_Login3"

#     b = pd.read_sql_query(query , cnxn)
#     b = pd.DataFrame(b)

#     if ( (b.loc[(b['Name'] == name) & (b['Email'] == email) & (b['Username'] == username) & (b['PasswordHash'] == password)]).shape[0] > 0 ):
#         result = "True"
#     else:
#         result = "False"


#     cursor.close()
#     cnxn.close()


#     return HttpResponse(result,  content_type="application/json")



def Register(request):
    name = request.GET.get('name')
    email = request.GET.get('email')
    username = request.GET.get('username')
    password = request.GET.get('password')
    color = request.GET.get('color')
    flag = request.GET.get('flag')
    IB = request.GET.get('IB')


    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    
    query = "select * from Dashboard_Login"

    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)

    if ( (b.loc[ (b['Email'] == email) ].shape[0] >= 1 )):
        result = "EmailE"

    elif ( (b.loc[  (b['Username'] == username)].shape[0] >= 1) ):
        result = "UsernameE"

    elif ( (b.loc[ (b['Email'] == email) & (b['Username'] == username)].shape[0] == 0)):
        if (flag=='T'):
            cursor.execute("INSERT INTO Dashboard_Login(Name , Email, Username , PasswordHash , IB)" "VALUES (?, ?, ?, ? , ?)", (name , email, username , password , IB))
            cnxn.commit()
            result = "Success"
        else:
            result = "Success"
        
    cursor.close()
    cnxn.close()
    return HttpResponse(result,  content_type="application/json")



def Put_CSS(request):

    username = request.GET.get('username')
    fieldname = request.GET.get('fieldname')
    fieldvalue = request.GET.get('fieldvalue')


    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "select * from CSS_Table"
    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)
    
    query = "select * from CSS_Table"


    if ( (b.loc[ (b['Username'] == username) ].shape[0] == 1 )):
        cursor.execute("UPDATE CSS_Table SET Fieldvalue=? WHERE Username=?", fieldvalue, username)
        cnxn.commit()
        result = "Success"


    elif ( ( b.loc[  (b['Username'] == username)].shape[0] == 0) ):
        cursor.execute("INSERT INTO CSS_Table(Username , Fieldname, Fieldvalue )" "VALUES (?, ?, ?)", (username , fieldname, fieldvalue))
        cnxn.commit()
        result = "Success"


    
        
    cursor.close()
    cnxn.close()
    return HttpResponse(result,  content_type="application/json")


def Get_CSS(request):
    username = request.GET.get('username')

    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "select * from CSS_Table"

    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)

    b = b.loc[ (b['Username'] == username)].tail(1)
    LIST = [ {'Username' : b['Username'].iloc[0]} , {'Fieldname' : b['Fieldname'].iloc[0]} , {'Fieldvalue' : b['Fieldvalue'].iloc[0]} ]
    print(LIST)
    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def US_Symbols(request):
    b = pd.read_csv('American_Symbols.csv')
    b.columns = ['value' , 'label']
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def IN_Symbols(request):
    b = pd.read_csv('Indian_Symbols.csv')
    b.columns = ['value' , 'label']
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def Account_Updates(request):
    r = redis.StrictRedis(host='127.0.0.1', port=6379, db=5)
    
    b = pickle.loads(r.get('Account_Updates'))
    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)


    return HttpResponse(jsonfiles,  content_type="application/json")



def Account_Summary(request):
    r = redis.StrictRedis(host='127.0.0.1', port=6379, db=5)
    
    b = pickle.loads(r.get('Account_Summary'))
    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)


    return HttpResponse(jsonfiles,  content_type="application/json")



def Portfolio(request):
    r = redis.StrictRedis(host='127.0.0.1', port=6379, db=5)
    
    b = pickle.loads(r.get('Portfolio'))
    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)


    return HttpResponse(jsonfiles,  content_type="application/json")



def trade(request):
    asyncio.set_event_loop(asyncio.new_event_loop())

    stock = request.GET.get('stock')
    tradetype = request.GET.get('tradetype')
    quantity = request.GET.get('quantity')

    try:
        ib = IB()
        # ib.connect('192.168.1.195', 7497, clientId=100)
        ib.connect('127.0.0.1', 7497, clientId=13)

        contract = Stock(stock, 'NSE', 'INR')
        order = MarketOrder(tradetype, quantity)
        ib.placeOrder(contract , order)
        jsonfiles = "Trade Sucessful"
    except Exception as e:
        jsonfiles = "Trade Unsucessful"
        print("Trade Not Completed")



    
    return HttpResponse(jsonfiles,  content_type="application/json")

@csrf_exempt
def postapi(request):
    print(request.POST.get('name'))
    name = request.POST.get('name')

   
    jsonfiles = "Post Request: " + str(name)
    return HttpResponse(jsonfiles,  content_type="application/json")


@csrf_exempt   
def emailapi(request):
    # print(request.POST.get('name'))
    # name = request.POST.get('name')
    receiver = request.POST.get('receiver')
    subject = request.POST.get('subject')
    body = request.POST.get('body')

    sender = 'nikhil@finflock.com'
    recipient = receiver
    msg = MIMEText("Message text")
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    # msg['From'] = sender
    msg['To'] = recipient

    html = body

    part2 = MIMEText(html, 'html')
    msg.attach(part2)
    server = smtplib.SMTP_SSL('smtp.zoho.com', 465)
    server.login('nikhil@finflock.com', 'Nikhil123!')
    server.sendmail(sender, [msg['To']], msg.as_string())
    server.quit()

   
    jsonfiles = "Email sent"
    return HttpResponse(jsonfiles,  content_type="application/json")



def ForgetPassword(request):
    username = request.GET.get('username')
    password = request.GET.get('password')
    
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    
    query = "select * from Dashboard_Login"

    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)

    # if ( (b.loc[ (b['Email'] == email) ].shape[0] >= 1 )):
    #     result = "EmailE"

    # elif ( (b.loc[  (b['Username'] == username)].shape[0] >= 1) ):
    #     result = "UsernameE"

    # elif ( (b.loc[ (b['Email'] == email) & (b['Username'] == username)].shape[0] == 0) ):
    cursor.execute("UPDATE Dashboard_Login SET PasswordHash=? WHERE Username=?", password, username)
    cnxn.commit()
    result = "Success"
        
    cursor.close()
    cnxn.close()
    return HttpResponse(result,  content_type="application/json")


def Put_Portfolio(request):

    Username = request.GET.get('Username')
    Sessionid = request.GET.get('Sessionid')
    Portfolio = request.GET.get('Portfolio')

    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=IBBROKERSERVICE;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "select * from Portfolio"
    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)
    # print(b)
    # bb = b.loc[b["Username"] == Username, ['Portfolio']]
    # bb = bb.iat[0,0]
    # bb = json.dumps(json.loads(bb), indent=4)
    # print(bb)
    
    if ( (b.loc[ (b['Username'] == Username) ].shape[0] == 1 )):
        cursor.execute("UPDATE Portfolio SET Portfolio=? WHERE Username=?", Portfolio, Username)
        cnxn.commit()
        result = "Success"


    elif ( ( b.loc[  (b['Username'] == Username)].shape[0] == 0) ):
        cursor.execute("INSERT INTO Portfolio(Username , Session_ID, Portfolio )" "VALUES (?, ?, ?)", (Username , Sessionid, Portfolio))
        cnxn.commit()
        result = "Success"
        
    cursor.close()
    cnxn.close()
    return HttpResponse(result,  content_type="application/json")


def Get_Portfolio(request):
    username = request.GET.get('username')

    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=IBBROKERSERVICE;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "select * from Portfolio" 

    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)

    b = b.loc[ (b['Username'] == username)].tail(1)
    # c = b.loc[b["Username"] == username, ['Portfolio']]
    # c = c.iat[0,0]
    # c = json.loads(c)
    # c = c[1]
    # c = json.dumps(c)
    # b['Portfolio'].iloc[0] = (b['Portfolio'].iloc[0],)
    print(b['Portfolio'].iloc[0])
    # print(c)
    LIST = [ {'Username' : b['Username'].iloc[0]} , {'Session_ID' : b['Session_ID'].iloc[0]} , {'Portfolio' : b['Portfolio'].iloc[0]} ]
    # print(LIST)
    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def Del_Portfolio(request):

    Username = request.GET.get('Username')
    Sessionid = request.GET.get('Sessionid')
    Entry = request.GET.get('Entry')

    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=IBBROKERSERVICE;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "select * from Portfolio"
    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)
    bb = b.loc[b["Username"] == Username, ['Portfolio']]
    bb = bb.iat[0,0]
    bb = json.loads(bb)
    print(bb)
    bb = bb[1]
    print(bb)
    d = json.loads(Entry)

    # for i in range(0, len(bb)):
    #     if(d == bb[i]):
    #         bb.pop(i)
    #         break

    bb = json.dumps(bb)

    cursor.execute("UPDATE Portfolio SET Portfolio=? WHERE Username=?", bb, Username)
    cnxn.commit()
    result = "Success"
        
    cursor.close()
    cnxn.close()
    return HttpResponse(result,  content_type="application/json")




def ib_test(request):

    data = pd.DataFrame()
    asyncio.set_event_loop(asyncio.new_event_loop())
    ib = IB()
    ib.connect('127.0.0.1', 7497, clientId=11)


    sub = ScannerSubscription(instrument='STOCK.HK',locationCode='STK.HK.NSE', scanCode='TOP_OPEN_PERC_GAIN')
    ib.reqScannerSubscriptionAsync(sub)
    x = ib.reqScannerData(sub, [])
    # print(x)
    for i in x:
        # print(i.contractDetails.contract.symbol)
        print(i)
        contract = Stock(i.contractDetails.contract.symbol , 'NSE', 'INR')
        bars = ib.reqHistoricalData(contract, endDateTime='', durationStr='30 D',
            barSizeSetting='1 day', whatToShow='MIDPOINT', useRTH=True)

        df = util.df(bars)
        df = df[['date', 'close']].tail(2)
        change = (df.iat[1,1] / df.iat[0,1]) - 1
        change = change * 100
        df = df.tail(1)
        df['change%'] = change
        df['change%'] = df['change%'].round(2)
        df['symbol'] = i.contractDetails.contract.symbol
        # df = df.iat[0,1]
        # print(df)
        data = data.append(df, ignore_index=True)

    data = data.head(10)
    data = data.to_json(orient='records')
    print(type(data))
    jsonfiles = json.dumps(json.loads(data)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")

def signalTest(request):
    region = request.GET.get('region')
    region = region.upper()
    stock = request.GET.get('stock')
    stock = stock.upper()
    if(region == 'IN'):
        symbol = str(stock) + ".NS"

    elif(region == 'US'):
        symbol = str(stock)

    print(symbol)
    startdate = str(date.today() - timedelta(days=180))
    enddate = str(date.today())
    yahoo_financials = YahooFinancials(symbol)
    data = yahoo_financials.get_historical_price_data(startdate, enddate, "daily")
    data1 = data[symbol]["prices"]
    a = json.dumps(data1)
    df = pd.read_json(a)
    df['Timestamp'] = df['date']
    print(df.tail())
    df = df[['Timestamp' , 'open' , 'high' , 'low' , 'adjclose' , 'volume']]
    df = add_all_ta_features(df, "open", "high", "low", "adjclose", "volume", fillna=True)
    df.fillna("No value", inplace = True)
    
    b = df.to_json(orient='records')
    b = json.loads(b)
    LIST = {'symbol': stock, 'indicators': b}
    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")

def indicator(request):
    region = request.GET.get('region')
    region = region.upper()
    stock = request.GET.get('stock')
    stock = stock.upper()
    days = request.GET.get('days')
    days = int(days)
    indicator = request.GET.get('indicator')
    indicator = indicator.lower()

    if(region == 'IN'):
        symbol = str(stock) + ".NS"

    elif(region == 'US'):
        symbol = str(stock)

    print(symbol)
    startdate = str(date.today() - timedelta(days=days))
    enddate = str(date.today())
    yahoo_financials = YahooFinancials(symbol)
    data = yahoo_financials.get_historical_price_data(startdate, enddate, "daily")
    data1 = data[symbol]["prices"]
    a = json.dumps(data1)
    df = pd.read_json(a)
    df['Timestamp'] = df['date']
    df = df[['Timestamp' , 'open' , 'high' , 'low' , 'adjclose' , 'volume']]
    print(df.tail())
    df = add_all_ta_features(df, "open", "high", "low", "adjclose", "volume", fillna=True)
    df['indicatorValue'] = df[indicator]
    df = df[['Timestamp', 'open', 'high', 'low', 'adjclose', 'volume', 'others_dr' , 'indicatorValue']]
    df.fillna("No value", inplace = True)
    
    b = df.to_json(orient='records')
    b = json.loads(b)
    LIST = {'symbol': stock,'indicator': indicator, 'data': b}
    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def technicalInput(request):
    region = request.GET.get('region')
    region = region.upper()
    stock = request.GET.get('stock')
    stock = stock.upper()
    days = request.GET.get('days')
    days = int(days)

    if(region == 'IN'):
        symbol = str(stock) + ".NS"

    elif(region == 'US'):
        symbol = str(stock)

    print(symbol)
    startdate = str(date.today() - timedelta(days=days))
    enddate = str(date.today())
    yahoo_financials = YahooFinancials(symbol)
    data = yahoo_financials.get_historical_price_data(startdate, enddate, "daily")
    data1 = data[symbol]["prices"]
    a = json.dumps(data1)
    df = pd.read_json(a)
    df['Timestamp'] = df['date']
    df = df[['Timestamp' , 'open' , 'high' , 'low' , 'adjclose' , 'volume']]
    print(df.tail())
    df = add_all_ta_features(df, "open", "high", "low", "adjclose", "volume", fillna=True)
    df.fillna("No value", inplace = True)
    
    b = df.to_json(orient='records')
    b = json.loads(b)
    LIST = {'symbol': stock, 'indicators': b}
    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def ib_ohlcv(request):
    # asyncio.set_event_loop(asyncio.new_event_loop())
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    ib = IB()
    ib.connect('127.0.0.1', 7497, clientId=9)

    name = request.GET.get('name')
    contract = Stock(name , 'NSE', 'INR')
    bars = ib.reqHistoricalData(contract, endDateTime='', durationStr='5 D',
            barSizeSetting='1 hour', whatToShow='TRADES', useRTH=True)
    df = util.df(bars)
    # loop = asyncio.get_running_loop()
    # loop.stop()
    # loop.close()
    # ib.disconnect()
    # df = df[['date', 'open', 'high', 'low', 'close']]
    b = df.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def Post_notification(request):

    username = request.GET.get('username')
    all_notif = request.GET.get('all_notif')

    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "select * from Notification"
    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)
    
    if ( (b.loc[ (b['username'] == username) ].shape[0] == 1 )):
        cursor.execute("UPDATE Notification SET all_notif=? WHERE username=?", all_notif, username)
        cnxn.commit()
        result = "Success"


    elif ( ( b.loc[  (b['username'] == username)].shape[0] == 0) ):
        cursor.execute("INSERT INTO Notification(username , all_notif )" "VALUES (?, ?)", (username , all_notif))
        cnxn.commit()
        result = "Success"
        
    cursor.close()
    cnxn.close()
    return HttpResponse(result,  content_type="application/json")


def Get_notification(request):
    username = request.GET.get('username')

    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "select * from Notification" 

    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)

    b = b.loc[ (b['username'] == username)].tail(1)
    # c = b.loc[b["Username"] == username, ['Portfolio']]
    # c = c.iat[0,0]
    # c = json.loads(c)
    # c = c[1]
    # c = json.dumps(c)
    # b['Portfolio'].iloc[0] = (b['Portfolio'].iloc[0],)
    print(b['all_notif'].iloc[0])
    # print(c)
    LIST = [ {'username' : b['username'].iloc[0]} , {'all_notif' : b['all_notif'].iloc[0]} ]
    # print(LIST)
    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")

def fred_index(request):
    index = request.GET.get('index')
    index = index.upper()
    print(index)
    days = request.GET.get('days')
    days = int(days)
    startdate = date.today() - timedelta(days=days)
    fred = Fred(api_key='0924f9b4aad1b427d07a0a30d10af16c')
    data = fred.get_series(index, observation_start=startdate)
    info = fred.get_series_info(index)
    value = list(data)
    time = list(data.index)
  
    df = pd.DataFrame({'value':value, 'time':time})
    
    b = df.to_json(orient='records')
    JSON = json.loads(b)
    LIST = [{ 'index' : info['id'], 'title' : info['title'], 'data': JSON }]
    LIST.append({ 'index' : 'asd', 'title' : 'mc', 'data': 'bc' })
    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")

def fredcharts(request):
    days = request.GET.get('days')
    days = int(days)
    startdate = date.today() - timedelta(days=days)
    indices = ['SP500', 'DJIA', 'WILL5000INDFC', 'NASDAQCOM', 'WILL5000PRFC', 'RU2000PR', 'RU3000TR', 'WILL5000PR']
    LIST = []
    for index in indices:
        print(index)
        fred = Fred(api_key='0924f9b4aad1b427d07a0a30d10af16c')
        data = fred.get_series(index, observation_start= startdate)
        info = fred.get_series_info(index)
        value = list(data)
        time = list(data.index)
    
        df = pd.DataFrame({'value':value, 'time':time})
        
        b = df.to_json(orient='records')
        JSON = json.loads(b)
        LIST.append({ 'index' : info['id'], 'title' : info['title'], 'data': JSON })

    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")

def historical_data(request):
    region = request.GET.get('region')
    region = region.upper()
    stock = request.GET.get('stock')
    stock = stock.upper()
    days = request.GET.get('days')
    days = int(days)
    if(region == 'IN'):
        symbol = str(stock) + ".NS"

    elif(region == 'US'):
        symbol = str(stock)
        
    startdate = str(date.today() - timedelta(days=days))
    enddate = str(date.today())
    yahoo_financials = YahooFinancials(symbol)
    data = yahoo_financials.get_historical_price_data(startdate, enddate, "daily")
    jsonfiles = json.dumps(data  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")

def financial_statements(request):
    region = request.GET.get('region')
    region = region.upper()
    stock = request.GET.get('stock')
    stock = stock.upper()
    statement = request.GET.get('statement')
    statement = statement.lower()
    frequency = request.GET.get('frequency')
    frequency = frequency.lower()
    if(region == 'IN'):
        symbol = str(stock) + ".NS"

    elif(region == 'US'):
        symbol = str(stock)

    print(symbol)
    yahoo_financials = YahooFinancials(symbol)
    data =  yahoo_financials.get_financial_stmts(frequency, statement)
    jsonfiles = json.dumps(data  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")

def stock_earnings(request):
    region = request.GET.get('region')
    region = region.upper()
    stock = request.GET.get('stock')
    stock = stock.upper()
    if(region == 'IN'):
        symbol = str(stock) + ".NS"

    elif(region == 'US'):
        symbol = str(stock)

    print(symbol)
    yahoo_financials = YahooFinancials(symbol)
    data =  yahoo_financials.get_stock_earnings_data()
    jsonfiles = json.dumps(data  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")

def summary_data(request):
    region = request.GET.get('region')
    region = region.upper()
    stock = request.GET.get('stock')
    stock = stock.upper()
    if(region == 'IN'):
        symbol = str(stock) + ".NS"

    elif(region == 'US'):
        symbol = str(stock)

    print(symbol)
    yahoo_financials = YahooFinancials(symbol)
    data =  yahoo_financials.get_summary_data()
    jsonfiles = json.dumps(data  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")

def statistics_data(request):
    region = request.GET.get('region')
    region = region.upper()
    stock = request.GET.get('stock')
    stock = stock.upper()
    if(region == 'IN'):
        symbol = str(stock) + ".NS"

    elif(region == 'US'):
        symbol = str(stock)

    print(symbol)
    yahoo_financials = YahooFinancials(symbol)
    data =  yahoo_financials.get_key_statistics_data()
    jsonfiles = json.dumps(data  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def financial_statements_all(request):
    region = request.GET.get('region')
    region = region.upper()
    statement = request.GET.get('statement')
    statement = statement.lower()
    frequency = request.GET.get('frequency')
    frequency = frequency.lower()

    if(region == 'IN'):
        b = pd.read_csv('Indian_Symbols.csv')
        b.columns = ['value' , 'label']
        symbols_data = list(b['value'])
        print(symbols_data)
        symbols = []
        for symbol in symbols_data:
            x = str(symbol) + ".NS"
            symbols.append(x)
        print(symbols)

    elif(region == 'US'):
        b = pd.read_csv('American_Symbols.csv')
        b.columns = ['value' , 'label']
        symbols = list(b['value'])
        print(symbols)

    
    # print(symbol)
    yahoo_financials = YahooFinancials(symbols)
    data =  yahoo_financials.get_financial_stmts(frequency, statement)
    jsonfiles = json.dumps(data  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def quandlData(request):
    # select input
    select = request.GET.get('select')
    # from input
    fro = request.GET.get('fro')
    # where input
    where = request.GET.get('where')
    print(select , fro , where)
    cnxn = pyodbc.connect('Driver={ODBC Driver 13 for SQL Server};Server=tcp:www.finflock.com\SQLEXPRESS,49172;Database=QUANDL;Uid=system.services;Pwd=Finflock123!;')
    cursor = cnxn.cursor()
    select = str(select)
    fro = str(fro)
    where = str(where)
    where = where.replace('-' , '=')
    SQL_String = "SELECT " + select + " FROM " + fro + " WHERE " + where
    print("THIS IS SQL QUERY : " , SQL_String)
    b = pd.read_sql_query(SQL_String , cnxn)
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def indicatorSignals(request):
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    region = request.GET.get('region')
    region = region.upper()
    stock = request.GET.get('stock')
    stock = stock.upper()
    days = request.GET.get('days')
    days = int(days)
    code = "indicatorSignal_" + str(stock)
    hash = hashlib.sha256(code.encode('utf-8')).hexdigest()
    # r.delete(hash)

    if(region == 'IN'):
        symbol = str(stock) + ".NS"

    elif(region == 'US'):
        symbol = str(stock)

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b  = pickle.loads(r.get(hash))

    else:
        startdate = str(date.today() - timedelta(days=days))
        enddate = str(date.today())
        yahoo_financials = YahooFinancials(symbol)
        data = yahoo_financials.get_historical_price_data(startdate, enddate, "daily")
        data1 = data[symbol]["prices"]
        a = json.dumps(data1)
        df = pd.read_json(a)
        df['Timestamp'] = df['date']
        print(df.tail())
        df = df[['Timestamp' , 'open' , 'high' , 'low' , 'adjclose' , 'volume']]
        df = add_all_ta_features(df, "open", "high", "low", "adjclose", "volume", fillna=True)
        df.fillna("No value", inplace = True)
        df['Symbol'] = stock

        Indicators_List = ['trend_macd_signal', 'trend_macd_diff', 'trend_ema_fast','trend_ema_slow', 'trend_adx', 'trend_adx_pos', 'trend_adx_neg', 'trend_vortex_ind_pos', 'trend_vortex_ind_neg','trend_vortex_diff', 'trend_trix', 'trend_mass_index', 'trend_cci','trend_dpo', 'trend_kst', 'trend_kst_sig', 'trend_kst_diff','trend_ichimoku_a', 'trend_ichimoku_b', 'momentum_rsi', 'momentum_mfi','momentum_tsi', 'momentum_uo', 'momentum_stoch','momentum_stoch_signal', 'momentum_wr', 'momentum_ao']

        Signal_List = ['MACD_Signal', 'MACD_Diff_Signal', 'EMA_Fast_Signal','EMA_Slow_Signal', 'ADX_Signal', 'ADX_Pos_Signal', 'ADX_Neg_Signal', 'Vortex_Pos_Signal', 'Vortex_Neg_Signal','Vortex_Diff_Signal', 'Trix_Signal', 'Mass_Signal', 'CCI_Signal','DPO_Signal', 'KST_Signal', 'KST_Sig_Signal', 'KST_Diff_Signal','ICHIMOKU_Signal', 'ICHIMOKU_B_Signal', 'RSI_Signal', 'MFI_Signal','TSI_Signal', 'UO_Signal', 'Stoch_Signal','Stoch_Sig_Signal', 'WR_Signal', 'AO_Signal']

        for ind in range(0,len(Signal_List)):
            ## STRATEGY
            List1 = list(df[Indicators_List[ind]].nlargest(round(df.shape[0]*0.2)))
            List2 = list(df[Indicators_List[ind]].nsmallest(round(df.shape[0]*0.2)))
            print(List1)
            print(List2)

            df[Signal_List[ind]] = 0

            for i in range(0, df.shape[0]):
                if (df[Indicators_List[ind]][i] in List1):
                    # print("Sell")
                    df[Signal_List[ind]][i] = "Sell"
                elif (df[Indicators_List[ind]][i] in List2):
                    # print("Buy")
                    df[Signal_List[ind]][i] = "Buy"
                else :
                    # print("Hold")
                    df[Signal_List[ind]][i] = "Hold"

        df = df[["Symbol" ,'Timestamp' , 'MACD_Signal', 'EMA_Fast_Signal',
                    'EMA_Slow_Signal', 'ADX_Signal',
                    'Vortex_Pos_Signal','Trix_Signal', 'Mass_Signal', 'CCI_Signal',
                    'DPO_Signal', 'KST_Signal',
                    'ICHIMOKU_Signal', 'RSI_Signal', 'MFI_Signal',
                    'TSI_Signal', 'UO_Signal', 'Stoch_Signal'
                    , 'WR_Signal', 'AO_Signal']]

        df.columns = ['Symbol' ,'Timestamp', 'MACD', 'EMA',
            'EMAS', 'ADX',
            'Vortex','Trix', 'Mass', 'CCI',
            'DPO', 'KST',
            'ICHIMOKU', 'RSI', 'MFI',
            'TSI', 'UO',
            'Stoch', 'WR', 'AO']


        b = df.tail(1)
        df['Timestamp'][0] = str(df['Timestamp'][0])
        print(b)
        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)

    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def technical_strategy_default(request):
    indicator = request.GET.get('indicator')

    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=TRADEENGINE;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "select * from technical_strategy_default_val WHERE indicator = " + "'" + indicator + "'"

    print(query)

    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)

    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def post_technical_strategy(request):

    username = request.GET.get('username')
    strategy = request.GET.get('strategy')
    ticker = request.GET.get('ticker')
    indicator = request.GET.get('indicator')
    buy_condition = request.GET.get('buy_condition')
    buy_value = request.GET.get('buy_value')
    buy_lowerbound = request.GET.get('buy_lowerbound')
    buy_upperbound = request.GET.get('buy_upperbound')
    buy_quantity = request.GET.get('buy_quantity')
    sell_condition = request.GET.get('sell_condition')
    sell_value = request.GET.get('sell_value')
    sell_lowerbound = request.GET.get('sell_lowerbound')
    sell_upperbound = request.GET.get('sell_upperbound')
    sell_quantity = request.GET.get('sell_quantity')
    only_once = request.GET.get('only_once')
    every_time = request.GET.get('every_time')
    expiration_date = request.GET.get('expiration_date')
    send_mail = request.GET.get('send_mail')
    show_popup = request.GET.get('show_popup')
    summary = request.GET.get('summary')


    # username = 'fddf'
    # strategy = None
    # ticker = 'fddf'
    # indicator = 'fddf'
    # buy_condition = 'fddf'
    # buy_value = 'fddf'
    # buy_lowerbound = 'fddf'
    # buy_upperbound = 'fddf'
    # buy_quantity = 'fddf'
    # sell_condition = 'fddf'
    # sell_value = 'fddf'
    # sell_lowerbound = 'fddf'
    # sell_upperbound = 'fddf'
    # sell_quantity = 'fddf'
    # only_once = 'fddf'
    # every_time = 'fddf'
    # expiration_date = 'fddf'
    # send_mail = 'fddf'
    # show_popup = 'fddf'
    # summary = 'fddf'

    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=TRADEENGINE;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    # query = "select * from Strategy"
    # b = pd.read_sql_query(query , cnxn)
    # b = pd.DataFrame(b)
    
    # if ( (b.loc[ (b['username'] == username) ].shape[0] == 1 )):
    #     cursor.execute("UPDATE Notification SET all_notif=? WHERE username=?", all_notif, username)
    #     cnxn.commit()
    #     result = "Success"


    # elif ( ( b.loc[  (b['username'] == username)].shape[0] == 0) ):
    #     cursor.execute("INSERT INTO Notification(username , all_notif )" "VALUES (?, ?)", (username , all_notif))
    #     cnxn.commit()
    #     result = "Success"

    cursor.execute("INSERT INTO Strategy(username , strategy, ticker, indicator, buy_condition, buy_value, buy_lowerbound, buy_upperbound, buy_quantity, sell_condition, sell_value, sell_lowerbound, sell_upperbound, sell_quantity, only_once, every_time, expiration_date, send_mail, show_popup, summary)" "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (username , strategy, ticker, indicator, buy_condition, buy_value, buy_lowerbound, buy_upperbound, buy_quantity, sell_condition, sell_value, sell_lowerbound, sell_upperbound, sell_quantity, only_once, every_time, expiration_date, send_mail, show_popup, summary))
    cnxn.commit()
    result = "Success"
        
    cursor.close()
    cnxn.close()
    return HttpResponse(result,  content_type="application/json")


def get_technical_strategy(request):
    username = request.GET.get('username')

    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=TRADEENGINE;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "select * from Strategy WHERE username = " + "'" + username + "'"

    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)

    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


'''

import asyncio
import datetime
import hashlib
import json
import pickle
## Importing Libraries
import sys
import time
import warnings
from copy import deepcopy
from datetime import date, timedelta

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import quandl
import redis
import requests
from bokeh.models import ColumnDataSource
from bokeh.plotting import figure, output_file, show
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from flask_cors import CORS
from ib_insync import *
from nsepy import get_history
from nsetools import Nse
from scipy.stats import zscore
from sklearn import linear_model, preprocessing
from sklearn.decomposition import PCA, FastICA, TruncatedSVD
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.metrics import (accuracy_score, confusion_matrix, f1_score,
                             precision_score, recall_score)
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import BernoulliRBM
from sklearn.random_projection import (GaussianRandomProjection,
                                       SparseRandomProjection)
from statsmodels.graphics.tsaplots import plot_pacf
from ta import *

import pyodbc

# from django.contrib.auth import authenticate
# from django.views.decorators.csrf import csrf_exempt
# from rest_framework.authtoken.models import Token
# from rest_framework.decorators import api_view, permission_classes
# from rest_framework.permissions import AllowAny
# from rest_framework.status import (
#     HTTP_400_BAD_REQUEST,
#     HTTP_404_NOT_FOUND,
#     HTTP_200_OK
# )
# from rest_framework.response import Response



# http://127.0.0.1:8000/api/?select=Date,Ratio_Name,Ratio_Value&fro=Indian_Fundamental_Ratios&where=%27TCS%27
def index(request):
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    select = request.GET.get('select')
    fro = request.GET.get('fro')
    where = request.GET.get('where')
    # print(select , fro , where)
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    select = str(select)
    fro = str(fro)
    where = str(where)
    where = where.replace('-' , '=')
    SQL_String = "SELECT " + select + " FROM " + fro + " WHERE " + where
    # print("THIS IS SQL QUERY : " , SQL_String)

    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)
    if(r.get(hash)):
        print("LOADED FROM CACHE")
        StartTime = time.time()
        b = pd.read_sql_query(SQL_String , cnxn)
        # print(b)
        EndTime = time.time()
        TotalTime = EndTime - StartTime
    else:
        b = pd.read_sql_query(SQL_String , cnxn)
        print("LOADED FROM DB")
        pickobj = pickle.dumps(b)
        
        r.set(hash , pickobj)
        # r.expire(hash , 100)
        b  = pickle.loads(r.get(hash))
        StartTime = time.time()

        


    # b = pd.read_sql_query(SQL_String , cnxn)
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    data = b.to_json()
    # a = [[1,2,3,4,5,6,7], [1,2,3,4,5,6,7]]
    return HttpResponse(jsonfiles,  content_type="application/json")
    # return JsonResponse(a, safe=False)



def nifty(request):
    index = request.GET.get('index')
    d1 = date.today()
    d2 = date.today() - timedelta(days=100)
    N50 = get_history(symbol=index,start=d2,end=d1,index=True)
    N50 = N50.iloc[-60:]
    a = [[1,2,3,4,5,6,7], [1,2,3,4,5,6,7]]
   
    N50['Date'] = N50.index
    # print(N50)
    jsonfiles = json.dumps(json.loads(N50.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def niftycharts(request):
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    code = "NiftyCharts"
    hash = hashlib.sha256(code.encode('utf-8')).hexdigest()

    d1 = date.today()
    d2 = date.today() - timedelta(days=100)
    N50 = get_history(symbol='NIFTY 50' ,start=d2,end=d1,index=True)
    N50I = get_history(symbol='NIFTY IT' ,start=d2,end=d1,index=True)
    N50A = get_history(symbol='NIFTY AUTO' ,start=d2,end=d1,index=True)
    N50B = get_history(symbol='NIFTY BANK' ,start=d2,end=d1,index=True)
    INDIAVIX = get_history(symbol='NIFTY MEDIA' ,start=d2,end=d1,index=True)
    CNX100 = get_history(symbol='NIFTY METAL' ,start=d2,end=d1,index=True)
    NI15 = get_history(symbol='NIFTY 200' ,start=d2,end=d1,index=True)
    CNXIT = get_history(symbol='NIFTY 500' ,start=d2,end=d1,index=True)

    

    NI15
    N50 = N50.iloc[-60:]
    N50I = N50I.iloc[-60:]
    N50A =N50A.iloc[-60:]
    N50B = N50B.iloc[-60:]

    INDIAVIX = INDIAVIX.iloc[-60:]
    CNX100 = CNX100.iloc[-60:]
    NI15 =NI15.iloc[-60:]
    CNXIT = CNXIT.iloc[-60:]
    N50['Date'] = N50.index
    a = list(N50['Close'])
    b = list(N50I['Close'])
    c = list(N50A['Close'])
    d = list(N50B['Close'])

    aa = list(INDIAVIX['Close'])
    bb = list(CNX100['Close'])
    cc = list(NI15['Close'])
    dd = list(CNXIT['Close'])

    e = N50['Close'].iloc[-1]
    f = str(N50I['Close'].iloc[-1])
    g = str(N50A['Close'].iloc[-1])
    l = str(N50B['Close'].iloc[-1])

    ee = INDIAVIX['Close'].iloc[-1]
    ff = str(CNX100['Close'].iloc[-1])
    gg = str(NI15['Close'].iloc[-1])
    ll = str(CNXIT['Close'].iloc[-1])

    j = []
    for i in range(0, N50.shape[0]):
        j.append(str(N50['Date'].iloc[i]))
    LIST = [{'Nifty50Close' : a} , {'NiftyITClose' : b} , {'NiftyAutoClose' : c} ,  {'NiftyBankClose' : d}, {'NIFTYMEDIAClose' : aa} , {'NIFTYMETALCloss' : bb} , {'NIFTY200Close' : cc} ,  {'NIFTY500Close' : dd} , {'Nifty50Close1' : e} ,  {'NiftyITClose1' : f} ,  {'NiftyAutoClose1' : g} ,  {'NiftyBankClose1' : l},{'NIFTYMEDIAClose1' : ee} ,  {'NIFTYMETALClose1' : ff} ,  {'NIFTY200Close1' : gg} ,  {'NIFTY500Close1' : ll}  ,  {'Date' : j}]
    
    # print(N50)
    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def forex(request):
    asyncio.set_event_loop(asyncio.new_event_loop())
    ib = IB()
    ib.connect('127.0.0.1', 7497, clientId=20)
    contract = Contract()
    contract.symbol = 'TCS'
    contract.secType = "STK"
    contract.currency = "INR"
    contract.exchange = "NSE"
    contract.primaryExchange = "NSE"

    queryTime = datetime.datetime.today()
    a = ib.reqHistoricalData(contract, queryTime,"2000 S", "30 secs", "MIDPOINT", 1, 1, False, [])
    To_Analyse = []
    for ticks in a:
        To_Analyse.append(ticks.open)
    b = pd.DataFrame(To_Analyse)
    ib.disconnect()
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    data = b.to_json()
    return HttpResponse(jsonfiles,  content_type="application/json")


def chart(request):
    a = [[1,2,3,4,5,6,7], [1,2,3,4,5,6,7]]
    a = json.dumps(a  , indent=4)
    # return JsonResponse(a, safe=False,  content_type="application/json")
    return HttpResponse(a,  content_type="application/json")


def TimeSeries(request):
    name = request.GET.get('name')
    queryTime = datetime.datetime.today()
    quandl.ApiConfig.api_key = 'Yc9EU38DA1ry-YHirE6Y'
    st = "NSE/" + str(name)
    a = quandl.get(st, start_date='2000-1-1', end_date=queryTime)
    jsonfiles = json.dumps(json.loads(a.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


## For HighCharts, Single Stock OHLC
def OHLC(request):
    List = []
    name = request.GET.get('name')
    # print("THIS IS NAME" , name)
    queryTime = datetime.datetime.today()
    quandl.ApiConfig.api_key = 'Yc9EU38DA1ry-YHirE6Y'
    st = "NSE/" + str(name)
    a = quandl.get(st, start_date='2000-1-1', end_date=queryTime)
    a['Date'] = a.index
    a['ts'] = a.Date.values.astype(np.int64)
    a['ts'] = a['ts']//1000000
    # print(a.head())
    for index, row in a.iterrows():
        List.append([ row['ts'] , row['Open'] , row['High'], row['Low'], row['Close'] ])
    
    List = json.dumps(List  , indent=4 , cls=DjangoJSONEncoder)
    return HttpResponse(List,  content_type="application/json")


## For HighCharts, Compare Multiple Stock (Close only)
def Compare(request):
    Final = []
    List = []
    stocks = request.GET.get('stocks')
    stocks = stocks.split(',')
    queryTime = datetime.datetime.today()
    quandl.ApiConfig.api_key = 'Yc9EU38DA1ry-YHirE6Y'
    for i in stocks:
        st = "NSE/" + str(i)
        a = quandl.get(st, start_date='2000-1-1', end_date=queryTime)
        # a = quandl.get(st, start_date='2000-1-1', end_date='2001-1-1')
        a['Date'] = a.index
        a['ts'] = a.Date.values.astype(np.int64)
        a['ts'] = a['ts']//1000000
        # print(a.head())
        for index, row in a.iterrows():
            List.append([ row['ts'] , row['Close'] ])
        dictt  = {'name' : i , 'data' : List}
        Final.append(dictt)
        # print(Final)
        List = []
    
    # List = json.dumps(Final  , indent=4, default=json_util.default)
    # return HttpResponse(Final,  content_type="application/json")

    a = json.dumps(Final  , indent=4)
    return HttpResponse(a,  content_type="application/json")

def Fundamental_Ratios(request):
    select = request.GET.get('select')
    fro = request.GET.get('fro')
    where = request.GET.get('where')
    # print(select , fro , where)
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    select = str(select)
    fro = str(fro)
    where = str(where)
    where = where.replace('-' , '=')
    SQL_String = "SELECT " + select + " FROM " + fro + " WHERE " + where
    # print("THIS IS SQL QUERY : " , SQL_String)
    b = pd.read_sql_query(SQL_String , cnxn)
    b = b.loc[b['Date'] == str(max(b['Date'])) ]

    df2 = pd.read_csv('./Fundamental_Ratios_desc.csv')
    # print(df2.head())
    b = pd.merge(b , df2 , on='Ratio_Name')
    b = b[['Ratio_Desc' , 'Ratio_Value']]
    b.columns = ['Description' , 'Value']
    # print(b.head(0))
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    data = b.to_json()
    # a = [[1,2,3,4,5,6,7], [1,2,3,4,5,6,7]]
    return HttpResponse(jsonfiles,  content_type="application/json")
    # return JsonResponse(a, safe=False)

def ml_models(request):
    a = request.GET.get('Symbol')
    b = request.GET.get('drop')
    c = request.GET.get('model')
    # print(a)
    # print(b)
    # print(c)
    query = "http://www.finflock.com/api/?select=End_Date,Abbreviation_Value&fro="+ b +"_Fundamentals_Financial_Statements&where=Company_Symbol='"+ a +"' AND Abbreviation='"+ c +"' AND Fiscal_Period_Type='Annual'"
    # print(query)
    html = requests.get(query).content
    df = pd.read_json(html)
    # print(df)

    df = df.sort_values('End_Date', ascending=True)
    df.reset_index(inplace=True)
    df = df[['End_Date' , 'Abbreviation_Value']]

    # table = [ {'End_Date' : list(df['End_Date'])} , {'Abbreviation_Value' : list(df['Abbreviation_Value'])  }  ]  
    # table = json.dumps(table)
    # table = df.to_json(orient='records')

    model = LinearRegression()
    model.fit(pd.DataFrame(df.index), pd.DataFrame(df['Abbreviation_Value']))
    m= max(df.index)  + 1
    # print("THIS IS INDEX" , df.index)
    scatter = list(df['Abbreviation_Value'])
    
    predict = model.predict(m)[0][0]
    initial = model.predict(0)[0][0]
    predict = round(predict, 2)
    initial = round(initial, 2)

    query2 = "http://www.finflock.com/api/?select=Company,ISIN_No,Code,Industry&fro=All_Stocks_Symbol_Data&where=Symbol='"+ a +str("'")
    html = requests.get(query2).content
    info = pd.read_json(html)

    # print(info.head())
    company = info.ix[0]['Company']
    isin = info.ix[0]['ISIN_No']
    code = info.ix[0]['Code']
    industry = str(info.ix[0]['Industry'])
    industry = industry.replace("amp;" , "")
    # print(company , isin , code , industry)
    # print(info)
    # LIST = [{'a' : a }, {'b' : b }, {'c' : c}, {'zipdf' : zipdf}, {'sym': MovingLine_Symbols} ,{ 'gain': MovingLine_Gain}, {'lose': MovingLine_Lose} , {'predict': predict}, {'initial': initial}, {'scatter': scatter}, {'m': m}, {'company': company}, {'isin': isin}, {'code': code}, {'industry': industry}]
    LIST = [{'company': str(company) }, {'isin': str(isin) }, {'code': str(code) }, {'industry': str(industry)}, {'scatter': scatter}, { 'm': m }, { 'predict': predict }, {'initial': initial}, {'table': df.to_json(orient='records') } ]
    # print(LIST)

    a = json.dumps(LIST , sort_keys=True, indent=4, separators=(',', ': '))
    # a.replace("\", "")
    a.replace("\"", "")
    # print(a)
    # print(type(a))
    return HttpResponse(a,  content_type="application/json")

    # jsonfiles = json.dumps(a , indent=4)
    # return HttpResponse(jsonfiles,  content_type="application/json")



def Tech_Analysis(request):
    name = request.GET.get('name')
    TCS = get_history(symbol=name, start=date(2015,1,1), end=date(2015,12,31))[['Open', 'High', 'Low', 'Close' , 'Volume']]
    # print(TCS)
    TCS['t1'] = TCS['Close'].shift(-1)
    TCS['t2'] = TCS['t1'] - TCS['Close']
    TCS['Target'] = np.where(TCS['t2'] < 0, 0, 1)
    TCS.drop(['t2' , 't1'] , axis=1 , inplace=True)
    TCS.join(pd.get_dummies(TCS['Target']))
    # print(TCS.head(10))
    df_trainY = TCS["Target"]
    df_trainX = TCS.drop("Target" , axis=1)
    x = df_trainX.values
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df_trainX = pd.DataFrame(x_scaled)
    X = df_trainX.iloc[:-1 :].values
    y = df_trainY.iloc[:-1].values
    XP =  df_trainX.iloc[-1: :].values
    n_comp = 20
    fastICA = FastICA(n_components = n_comp)
    fastICA.fit(X)
    X = fastICA.transform(X)
    clf1 = LogisticRegression(random_state=1)
    clf2 = XGBClassifier(random_state=1)
    clf3 = GaussianNB()
    model = VotingClassifier(estimators=[('lr', clf1), ('rf', clf2), ('gnb', clf3)], voting='soft')
    # print(len(X))
    # print(len(y))
    model.fit(X, y)
    y_pred = model.predict_proba(XP)
    a = "\nThere are {}% chances that the {} stock price will go up".format(y_pred[0][1]*100 , "TCS")

    return HttpResponse(a,  content_type="application/json")



def sectors(request):
    name = request.GET.get('name')
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    # query = "Select Distinct All_Stocks_Symbol_Data.Symbol, All_Stocks_Symbol_Data.Industry, All_Stocks_Symbol_Data.ISIN_No, All_Stocks_Symbol_Data.Status FROM All_Stocks_Symbol_Data INNER JOIN  Indian_Fundamentals_General_Ratios ON All_Stocks_Symbol_Data.Symbol = Indian_Fundamentals_General_Ratios.Company_Symbol where [Status]  LIKE '%Active%' AND ( Industry LIKE '%"+str(name) +"%')"

    query = "Select Distinct All_Stocks_Symbol_Data.Symbol, All_Stocks_Symbol_Data.Industry, All_Stocks_Symbol_Data.ISIN_No, All_Stocks_Symbol_Data.Status, Indian_Fundamentals_Financial_Statements.Abbreviation_Value FROM All_Stocks_Symbol_Data INNER JOIN  Indian_Fundamentals_Financial_Statements ON All_Stocks_Symbol_Data.Symbol = Indian_Fundamentals_Financial_Statements.Company_Symbol where [Status]  LIKE '%Active%' AND ( Industry LIKE '%"+str(name) +"%' ) AND Abbreviation='SREV' AND Fiscal_Year=2017 AND Fiscal_Period_Number=0"


    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)
    b['Abbreviation_Value'] = b['Abbreviation_Value'].astype(float).round(2)
    for i in b.index:
        b['Industry'][i] = b['Industry'][i].replace("amp;" , "")
        # print(b['Industry'][i])
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


# Select Distinct All_Stocks_Symbol_Data.Symbol, All_Stocks_Symbol_Data.Industry, All_Stocks_Symbol_Data.ISIN_No, All_Stocks_Symbol_Data.Status,Indian_Fundamentals_Financial_Statements.Abbreviation_Value FROM All_Stocks_Symbol_Data INNER JOIN  Indian_Fundamentals_Financial_Statements ON All_Stocks_Symbol_Data.Symbol = Indian_Fundamentals_Financial_Statements.Company_Symbol 
# where [Status]  LIKE '%Active%' AND ( Industry LIKE '%health%') AND Abbreviation='SREV' AND Fiscal_Year=2017 AND Fiscal_Period_Number=0

def LR(request):
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "Select * FROM American_LinearRegression"
    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")

## For Stock Quotes
def quotes(request):
    List = []
    name = request.GET.get('name')
    d1 = date.today() - timedelta(days=2)
    b = get_history(symbol=name, start=date(2015,1,10), end=date(2025,1,10))
    b = b.tail(1)
    b['Change'] = ((b['Last'] - b['Prev Close'])/b['Last'])*100
    b['ChangeValue'] = ((b['Last'] - b['Prev Close']))
    b['Change'] = b['Change'].round(2)
    b['Prev_Close'] = b['Prev Close']
    b['Deliverable_Volume'] = b['Deliverable Volume']
    b.drop(['Deliverable Volume' , 'Prev Close'] , axis=1 , inplace=True)
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    data = b.to_json()
    return HttpResponse(jsonfiles,  content_type="application/json")

def US_Symbols(request):
    b = pd.read_csv('American_Symbols.csv')
    b.columns = ['value' , 'label']
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def IN_Symbols(request):
    b = pd.read_csv('Indian_Symbols.csv')
    b.columns = ['value' , 'label']
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def IB_News(request):
    name = request.GET.get('name')
    ALL_LIST = []
    # def get_news(company_symbol, exchange='SMART', currency='USD'):
    asyncio.set_event_loop(asyncio.new_event_loop())
    ib = IB()
    ib.connect('127.0.0.1', 7777, clientId=100)
    company_symbol = name
    exchange = 'SMART'
    currency = 'USD'

    stock = Stock(company_symbol, exchange, currency)
    newsProviders = ib.reqNewsProviders()
    codes = '+'.join(np.code for np in newsProviders)
    ib.qualifyContracts(stock)
    end_date = datetime.date.today() - datetime.timedelta(days=1)
    start_date = end_date - datetime.timedelta(days=30)
    headlines = ib.reqHistoricalNews(stock.conId, codes, str(start_date), str(end_date), 100)
    # print(start_date, end_date, len(headlines))
    headlines_mod = []
    for i in range(len(headlines)):
        _time = datetime.date(int(headlines[i].time.split(' ')[0].split('-')[0]), 
                            int(headlines[i].time.split(' ')[0].split('-')[1]), int(headlines[i].time.split(' ')[0].split('-')[2]))
        if _time <= end_date and _time >= start_date:
            headlines_mod.append(headlines[i])
    # print(len(headlines_mod))
    articles = []
    for i in range(len(headlines_mod)):
        articles.append(ib.reqNewsArticle(headlines_mod[i].providerCode, headlines_mod[i].articleId).articleText)
    time = []
    headline = []
    for i in range(len(headlines_mod)):
        headline.append(headlines_mod[i].headline)
        time.append(headlines_mod[i].time)
    # ib.disconnect()
    ALL_LIST.append(time)
    ALL_LIST.append(headline)
    ALL_LIST.append(articles)
    # print(ALL_LIST)
    ib.disconnect()
    b = pd.DataFrame({'Date': ALL_LIST[0] , 'Headline': ALL_LIST[1] , 'Article': ALL_LIST[2]})
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def FD_Compare(request):
    symbols = request.GET.get('symbols')
    symbols = symbols.split(",")
    df = pd.read_csv('Fundamental_Data(Annual).csv')
    Final_df = pd.DataFrame()

    # symbols  =['A' , 'AA' , 'AAPL' , 'AABA']
    for i in symbols:
        # print(str(i))
        temp = df.loc[(df['ticker'] == str(i))].tail(1)[['ticker' , 'assets' , 'debt' , 'evebitda' , 'marketcap' , 'liabilities' , 'eps']]
        Final_df = Final_df.append(temp)

    # print(Final_df)

    Final_df['assets_zscore'] = (Final_df['assets'] - Final_df['assets'].mean())/Final_df['assets'].std(ddof=0)
    Final_df['debt_zscore'] = (Final_df['debt'] - Final_df['debt'].mean())/Final_df['debt'].std(ddof=0)
    Final_df['evebitda_zscore'] = (Final_df['evebitda'] - Final_df['evebitda'].mean())/Final_df['evebitda'].std(ddof=0)
    Final_df['marketcap_zscore'] = (Final_df['marketcap'] - Final_df['marketcap'].mean())/Final_df['marketcap'].std(ddof=0)
    Final_df['liabilities_zscore'] = (Final_df['liabilities'] - Final_df['liabilities'].mean())/Final_df['liabilities'].std(ddof=0)
    Final_df['eps_zscore'] = (Final_df['eps'] - Final_df['eps'].mean())/Final_df['eps'].std(ddof=0)



    Final_df = Final_df[['ticker', 'assets', 'assets_zscore', 'debt', 'debt_zscore', 'evebitda', 'evebitda_zscore', 'marketcap' , 'marketcap_zscore', 'liabilities', 'liabilities_zscore', 'eps', 'eps_zscore' ]]
    b = Final_df
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def news(request):
    from newsapi import NewsApiClient
    newsapi = NewsApiClient(api_key='592ad2ec918f40f39a479b7acdb2d416')
    top_headlines = newsapi.get_top_headlines(language='en' , sources='google-news')
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    name = request.GET.get('name')

    SQL_String = "News " + str(name)
    # print("THIS IS SQL QUERY : " , SQL_String)

    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)
    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b = pickle.loads(r.get(hash))
        # print(b)

    else:
        print("LOADED FROM DB")
        from newsapi import NewsApiClient
        import pandas as pd
        from textblob import TextBlob
        newsapi = NewsApiClient(api_key='592ad2ec918f40f39a479b7acdb2d416')
        top_headlines = newsapi.get_top_headlines(language='en' , sources=name)
        LIST = []
        for i in range(0,len(top_headlines['articles'])-1):
            a = str(top_headlines['articles'][i]['description'])
            # a = "Good happy"
            blob = TextBlob(a)
            LIST.append([ top_headlines['articles'][i]['publishedAt'] , top_headlines['articles'][i]['title'] , top_headlines['articles'][i]['description'] , blob.sentiment.polarity ])
            # print()
            
        b = pd.DataFrame( LIST )    
        b.columns = ['Date' , 'Headline' , 'Discription' , 'Sentiments']
        # print(b)

        for m,i in enumerate(b.Sentiments):
            a = b.Sentiments[m]
            try:
                if (a < 0):
                    b.Sentiments[m] = "Negative"

                elif (a > 0):
                    b.Sentiments[m] = "Positive"

                else:
                    b.Sentiments[m] = "Neutral"
            except:
                pass



        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        r.expire(hash , 10*60*60)
        b  = pickle.loads(r.get(hash))

    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    data = b.to_json()
    return HttpResponse(jsonfiles,  content_type="application/json")



def fred(request):
    name = request.GET.get('name')
    quandl.ApiConfig.api_key = 'Yc9EU38DA1ry-YHirE6Y'
    quandl_code = "FRED/" + str(name)
    b = quandl.get(quandl_code)
    # print(b.head())
    b['Date'] = b.index
    # b = b[['Date' , 'Value']]
    b.columns = ['Value' , 'Date']
    # print(b.head())
    jsonfiles = json.dumps(json.loads(b.to_json(orient='records'))  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def fred2(request):
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    name = request.GET.get('name')
    quandl.ApiConfig.api_key = 'Yc9EU38DA1ry-YHirE6Y'
    quandl_code = "FRED/" + str(name)

    hash = hashlib.sha256(quandl_code.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)
    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b  = pickle.loads(r.get(hash))
        b.round(2)
       

    else:
        print("LOADED FROM DB")
        b = quandl.get(quandl_code, start_date='2014-10-01', end_date='2018-10-01')
        # print(b.head())
        b['Date'] = b.index
        # b = b[['Date' , 'Value']]
        b.columns = ['Value' , 'Date']
        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        b  = pickle.loads(r.get(hash))
        b.round(2)
        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        b  = pickle.loads(r.get(hash))
        # r.expire(hash , 100)
        # for i in range(0,b.shape[0]):
        #     b['Date'][i] = str(b['Date'][i]).split(" ")[0]
        # print(b.head())

        # b['TimeStamp'] = 0
        # # print(b.head())
        # for i in range(0, b.shape[0]):
        #     s = str(b['Date'][i]).split(" ")[0]
        #     # print(s)
        #     b['TimeStamp'][i] = time.mktime(datetime.datetime.strptime(s, "%Y-%d-%m").timetuple())
    LIST1 = []
    LIST2 = []
    
    for i in range(0,b.shape[0]):
        LIST1.append( str(b['Date'][i]).split(" ")[0] )
        LIST2.append( b['Value'][i])


    LIST = [{'Date' : LIST1} , {'Value' : LIST2} ,  {'Last_Price' : LIST2[-1]},  {'Change' : (LIST2[-1] - LIST2[-2])},  {'Per_Change' : ((LIST2[-1] - LIST2[-2])/ LIST2[-2])*100} ]


    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")




def screener(request):
    nse = Nse()

    name = request.GET.get('name')
    if name == 'gainers':
        b = nse.get_top_gainers()
    elif name == 'losers':
        b = nse.get_top_losers()

    b = pd.DataFrame(b)
    b['Change'] = ((b['highPrice'] - b['lowPrice'])/b['highPrice'])*100
    b['ChangeValue'] = ((b['highPrice'] - b['lowPrice']))
    b['Change'] = b['Change'].round(2)
    b = b[['symbol' , 'Change' , 'ChangeValue' , 'ltp']]
    

    b = b.to_json(orient='records')
    # print(type(b))
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def signals(request):
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    name = request.GET.get('name')
    code = "Signal_" + str(name)
    hash = hashlib.sha256(code.encode('utf-8')).hexdigest()
    # r.delete(hash)

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b  = pickle.loads(r.get(hash))

    else:
        df = get_history(symbol=name, start=date(2018,9,1), end=date(2028,10,10))
        df['Timestamp'] = df.index
        df['Symbol'] = name
        df = df[['Timestamp' , 'Open' , 'High' , 'Low' , 'Close' , 'Volume']]
        df.reset_index(drop=True, inplace=True)
        df = add_all_ta_features(df, "Open", "High", "Low", "Close", "Volume", fillna=True)
        df['Symbol'] = name
        # print(df.head())


        Indicators_List = ['trend_macd_signal', 'trend_macd_diff', 'trend_ema_fast','trend_ema_slow', 'trend_adx', 'trend_adx_pos', 'trend_adx_neg','trend_adx_ind', 'trend_vortex_ind_pos', 'trend_vortex_ind_neg','trend_vortex_diff', 'trend_trix', 'trend_mass_index', 'trend_cci','trend_dpo', 'trend_kst', 'trend_kst_sig', 'trend_kst_diff','trend_ichimoku_a', 'trend_ichimoku_b', 'momentum_rsi', 'momentum_mfi','momentum_tsi', 'momentum_uo', 'momentum_stoch','momentum_stoch_signal', 'momentum_wr', 'momentum_ao']

        Signal_List = ['MACD_Signal', 'MACD_Diff_Signal', 'EMA_Fast_Signal','EMA_Slow_Signal', 'ADX_Signal', 'ADX_Pos_Signal', 'ADX_Neg_Signal','ADX_Ind_Signal', 'Vortex_Pos_Signal', 'Vortex_Neg_Signal','Vortex_Diff_Signal', 'Trix_Signal', 'Mass_Signal', 'CCI_Signal','DPO_Signal', 'KST_Signal', 'KST_Sig_Signal', 'KST_Diff_Signal','ICHIMOKU_Signal', 'ICHIMOKU_B_Signal', 'RSI_Signal', 'MFI_Signal','TSI_Signal', 'UO_Signal', 'Stoch_Signal','Stoch_Sig_Signal', 'WR_Signal', 'AO_Signal']

        for ind in range(0,len(Signal_List)):
            ## STRATEGY
            List1 = list(df[Indicators_List[ind]].nlargest(round(df.shape[0]*0.2)))
            List2 = list(df[Indicators_List[ind]].nsmallest(round(df.shape[0]*0.2)))

            df[Signal_List[ind]] = 0

            for i in range(0, df.shape[0]):
                if (df[Indicators_List[ind]][i] in List1):
                    # print("Sell")
                    df[Signal_List[ind]][i] = "Sell"
                elif (df[Indicators_List[ind]][i] in List2):
                    # print("Buy")
                    df[Signal_List[ind]][i] = "Buy"
                else :
                    # print("Hold")
                    df[Signal_List[ind]][i] = "Hold"

        # print(df.head())
        # df = df[["Symbol" , "Timestamp" , "Open", "High", "Low", "Close", "Volume" ,'MACD_Signal', 'MACD_Diff_Signal', 'EMA_Fast_Signal',
        #     'EMA_Slow_Signal', 'ADX_Signal', 'ADX_Pos_Signal', 'ADX_Neg_Signal',
        #     'ADX_Ind_Signal', 'Vortex_Pos_Signal', 'Vortex_Neg_Signal',
        #     'Vortex_Diff_Signal', 'Trix_Signal', 'Mass_Signal', 'CCI_Signal',
        #     'DPO_Signal', 'KST_Signal', 'KST_Sig_Signal', 'KST_Diff_Signal',
        #     'ICHIMOKU_Signal', 'ICHIMOKU_B_Signal', 'RSI_Signal', 'MFI_Signal',
        #     'TSI_Signal', 'UO_Signal', 'Stoch_Signal',
        #     'Stoch_Sig_Signal', 'WR_Signal', 'AO_Signal']]

        df = df[["Symbol" ,'Timestamp' , 'MACD_Signal', 'EMA_Fast_Signal',
                    'EMA_Slow_Signal', 'ADX_Signal',
                    'Vortex_Pos_Signal','Trix_Signal', 'Mass_Signal', 'CCI_Signal',
                    'DPO_Signal', 'KST_Signal',
                    'ICHIMOKU_Signal', 'RSI_Signal', 'MFI_Signal',
                    'TSI_Signal', 'UO_Signal', 'Stoch_Signal'
                    , 'WR_Signal', 'AO_Signal']]

        df.columns = ['Symbol' ,'Timestamp', 'MACD', 'EMA',
            'EMAS', 'ADX',
            'Vortex','Trix', 'Mass', 'CCI',
            'DPO', 'KST',
            'ICHIMOKU', 'RSI', 'MFI',
            'TSI', 'UO',
            'Stoch', 'WR', 'AO']
        for i in range(0 , df.shape[0]):
            df['Timestamp'][i] = str(df['Timestamp'][i])


        b = df
        # print(b)
        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        b  = pickle.loads(r.get(hash))

    b = b.to_json(orient='records')
        # print(type(b))
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")






def signals_summary(request):
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    name = request.GET.get('name')
    code = "Signal_summary" + str(name)
    hash = hashlib.sha256(code.encode('utf-8')).hexdigest()
    # r.delete(hash)

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        LIST  = pickle.loads(r.get(hash))

    else:
        df = get_history(symbol=name, start=date(2018,9,1), end=date(2028,10,10))
        df['Timestamp'] = df.index
        df['Symbol'] = name
        df = df[['Timestamp' , 'Open' , 'High' , 'Low' , 'Close' , 'Volume']]
        df.reset_index(drop=True, inplace=True)
        df = add_all_ta_features(df, "Open", "High", "Low", "Close", "Volume", fillna=True)
        df['Symbol'] = name
        # print(df.head())


        Indicators_List = ['trend_macd_signal', 'trend_macd_diff', 'trend_ema_fast','trend_ema_slow', 'trend_adx', 'trend_adx_pos', 'trend_adx_neg','trend_adx_ind', 'trend_vortex_ind_pos', 'trend_vortex_ind_neg','trend_vortex_diff', 'trend_trix', 'trend_mass_index', 'trend_cci','trend_dpo', 'trend_kst', 'trend_kst_sig', 'trend_kst_diff','trend_ichimoku_a', 'trend_ichimoku_b', 'momentum_rsi', 'momentum_mfi','momentum_tsi', 'momentum_uo', 'momentum_stoch','momentum_stoch_signal', 'momentum_wr', 'momentum_ao']

        Signal_List = ['MACD_Signal', 'MACD_Diff_Signal', 'EMA_Fast_Signal','EMA_Slow_Signal', 'ADX_Signal', 'ADX_Pos_Signal', 'ADX_Neg_Signal','ADX_Ind_Signal', 'Vortex_Pos_Signal', 'Vortex_Neg_Signal','Vortex_Diff_Signal', 'Trix_Signal', 'Mass_Signal', 'CCI_Signal','DPO_Signal', 'KST_Signal', 'KST_Sig_Signal', 'KST_Diff_Signal','ICHIMOKU_Signal', 'ICHIMOKU_B_Signal', 'RSI_Signal', 'MFI_Signal','TSI_Signal', 'UO_Signal', 'Stoch_Signal','Stoch_Sig_Signal', 'WR_Signal', 'AO_Signal']

        for ind in range(0,len(Signal_List)):
            ## STRATEGY
            List1 = list(df[Indicators_List[ind]].nlargest(round(df.shape[0]*0.2)))
            List2 = list(df[Indicators_List[ind]].nsmallest(round(df.shape[0]*0.2)))

            df[Signal_List[ind]] = 0

            for i in range(0, df.shape[0]):
                if (df[Indicators_List[ind]][i] in List1):
                    df[Signal_List[ind]][i] = "Sell"
                elif (df[Indicators_List[ind]][i] in List2):
                    df[Signal_List[ind]][i] = "Buy"
                else :
                    df[Signal_List[ind]][i] = "Hold"


        df = df[["Symbol" ,'Timestamp' , 'MACD_Signal', 'EMA_Fast_Signal',
                    'EMA_Slow_Signal', 'ADX_Signal',
                    'Vortex_Pos_Signal','Trix_Signal', 'Mass_Signal', 'CCI_Signal',
                    'DPO_Signal', 'KST_Signal',
                    'ICHIMOKU_Signal', 'RSI_Signal', 'MFI_Signal',
                    'TSI_Signal', 'UO_Signal', 'Stoch_Signal'
                    , 'WR_Signal', 'AO_Signal']]

        df.columns = ['Symbol' ,'Timestamp', 'MACD', 'EMA',
            'EMAS', 'ADX',
            'Vortex','Trix', 'Mass', 'CCI',
            'DPO', 'KST',
            'ICHIMOKU', 'RSI', 'MFI',
            'TSI', 'UO',
            'Stoch', 'WR', 'AO']
        for i in range(0 , df.shape[0]):
            df['Timestamp'][i] = str(df['Timestamp'][i])


        b = df
        b = b.tail(1)
        
        # print(b)
        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)

        blist = b.values.tolist()
        # print("This is BList" , blist)
        bb1 = blist[0].count("Buy")
        bb2 = blist[0].count("Sell")
        bb3 = blist[0].count("Hold")
        if (bb1%2 == 0):
            o1 = bb1//2
            o2 = bb1//2
        else:
            o1 = bb1//2
            o2 = bb1//2 + 1

        if (bb2%2 == 0):
            m1 = bb2//2
            m2 = bb2//2
        else:
            m1 = bb2//2
            m2 = bb2//2 + 1

        if (bb3%2 == 0):
            h1 = bb3//2
            h2 = bb3//2
        else:
            h1 = bb3//2
            h2 = bb3//2 + 1

        LIST = [ {'O_Buy' : o1} , {'O_Sell' : m1} , {'O_Hold' : h1} , {'MA_Buy' : o2} , {'MA_Sell' : m2} , {'MA_Hold' : h2} , {'S_Buy' : bb1} , {'S_Sell' : bb2} , {'S_Hold' : bb3} ]

        pickobj = pickle.dumps(LIST)
        r.set(hash , pickobj)
        # r.expire(hash ,2* 60*60*24)
        LIST  = pickle.loads(r.get(hash))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")






def signals_all(request):
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    code = "Signal_All"
    hash = hashlib.sha256(code.encode('utf-8')).hexdigest()
    # r.delete(hash)

    if(1):
        print("LOADED FROM CACHE")
        b = pd.read_csv('Signals_Summary_All.csv')
        # b  = pickle.loads(r.get(hash))

    else:
        Final_df = pd.DataFrame()
        name = ['TCS' , 'WIPRO' , 'GAIL' , 'INFY' , 'TATAMOTOR' ]
        for n in name:
            try:
                df = pd.DataFrame()
                df = get_history(symbol=n, start=date(2018,10,25), end=date(2028,10,10))
                df['Timestamp'] = df.index
                df['Symbol'] = n
                df = df[['Symbol', 'Timestamp' , 'Open' , 'High' , 'Low' , 'Close' , 'Volume']]
                df.reset_index(drop=True, inplace=True)
                # print(df.head())
                df = add_all_ta_features(df, "Open", "High", "Low", "Close", "Volume", fillna=True)
        
                


                Indicators_List = ['trend_macd_signal', 'trend_macd_diff', 'trend_ema_fast','trend_ema_slow', 'trend_adx', 'trend_adx_pos', 'trend_adx_neg','trend_adx_ind', 'trend_vortex_ind_pos', 'trend_vortex_ind_neg','trend_vortex_diff', 'trend_trix', 'trend_mass_index', 'trend_cci','trend_dpo', 'trend_kst', 'trend_kst_sig', 'trend_kst_diff','trend_ichimoku_a', 'trend_ichimoku_b', 'momentum_rsi', 'momentum_mfi','momentum_tsi', 'momentum_uo', 'momentum_stoch','momentum_stoch_signal', 'momentum_wr', 'momentum_ao']

                Signal_List = ['MACD_Signal', 'MACD_Diff_Signal', 'EMA_Fast_Signal','EMA_Slow_Signal', 'ADX_Signal', 'ADX_Pos_Signal', 'ADX_Neg_Signal','ADX_Ind_Signal', 'Vortex_Pos_Signal', 'Vortex_Neg_Signal','Vortex_Diff_Signal', 'Trix_Signal', 'Mass_Signal', 'CCI_Signal','DPO_Signal', 'KST_Signal', 'KST_Sig_Signal', 'KST_Diff_Signal','ICHIMOKU_Signal', 'ICHIMOKU_B_Signal', 'RSI_Signal', 'MFI_Signal','TSI_Signal', 'UO_Signal', 'Stoch_Signal','Stoch_Sig_Signal', 'WR_Signal', 'AO_Signal']

                for ind in range(0,len(Signal_List)):
                    ## STRATEGY
                    List1 = list(df[Indicators_List[ind]].nlargest(round(df.shape[0]*0.2)))
                    List2 = list(df[Indicators_List[ind]].nsmallest(round(df.shape[0]*0.2)))

                    df[Signal_List[ind]] = 0

                    for i in range(0, df.shape[0]):
                        if (df[Indicators_List[ind]][i] in List1):
                            # print("Sell")
                            df[Signal_List[ind]][i] = "Sell"
                        elif (df[Indicators_List[ind]][i] in List2):
                            # print("Buy")
                            df[Signal_List[ind]][i] = "Buy"
                        else :
                            # print("Hold")
                            df[Signal_List[ind]][i] = "Hold"

                # print(df.head())
                df = df[["Symbol", 'Timestamp' , 'MACD_Signal', 'EMA_Fast_Signal',
                    'EMA_Slow_Signal', 'ADX_Signal',
                    'Vortex_Pos_Signal','Trix_Signal', 'Mass_Signal', 'CCI_Signal',
                    'DPO_Signal', 'KST_Signal',
                    'ICHIMOKU_Signal', 'RSI_Signal', 'MFI_Signal',
                    'TSI_Signal', 'UO_Signal', 'Stoch_Signal'
                    , 'WR_Signal', 'AO_Signal']]

                df.columns = ['Symbol', 'Timestamp' , 'MACD', 'EMA',
                    'EMAS', 'ADX',
                    'Vortex','Trix', 'Mass', 'CCI',
                    'DPO', 'KST',
                    'ICHIMOKU', 'RSI', 'MFI',
                    'TSI', 'UO',
                    'Stoch', 'WR', 'AO']
                # print(df.head())
                

                Final_df = Final_df.append(df)
                # print(Final_df['Symbol'].unique())
            except:
                pass
        maxt = max(Final_df['Timestamp'])
        Final_df = Final_df.loc[df['Timestamp'] == maxt]
        Final_df = Final_df[['Symbol' ,'Timestamp', 'MACD', 'EMA',
        'EMAS', 'ADX',
         'Vortex','Trix', 'Mass', 'CCI',
        'DPO', 'KST',
        'ICHIMOKU', 'RSI', 'MFI',
        'TSI', 'UO',
        'Stoch', 'WR', 'AO']]
        # print(Final_df.head())
        # print(Final_df.shape)
        
        b = Final_df
        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        b  = pickle.loads(r.get(hash))
    # print(b)
    b.reset_index(drop=True, inplace=True)
    for i in range(0 , b.shape[0]):
            b['Timestamp'][i] = str(b['Timestamp'][i])
    print("DONE")
    # b = b.set_index(['Symbol'])
    b = b.to_json(orient='records')
        # print(type(b))
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def ib(request):
    All_List = []
    asyncio.set_event_loop(asyncio.new_event_loop())
    ib = IB()
    ib.connect('127.0.0.1', 7497, clientId=100)
    a = ib.reqPositions()
    for i in a:
        All_List.append([i.account , i.contract.secType  , i.contract.conId , i.contract.symbol , i.contract.currency , i.contract.localSymbol , i.contract.tradingClass , i.position , i.avgCost])
    
    b = pd.DataFrame(All_List)
    
    b.columns = ['Account' , 'SecType' , 'ID' , 'Symbol' , 'Currency' , 'LocalSymbol' , 'TradingClass' , 'Position' , 'BuyPrice']
   
    b['CurrentPrice'] = 0
    b['Change'] = 0
    # b['Change (%)'] = 0
    b['P&L'] = 0
    for i in range(0, b.shape[0]):
        try:
            if (b['SecType'][i]=="STK"):

                c = Stock(b['Symbol'][i], 'NSE', 'INR')
                ib.reqMarketDataType(4)
                ticker = ib.reqMktData(c, '', False, False, None)
                ib.sleep(1)
                # print(ticker)
                # print(ticker.last)
                # print((ticker.last - b['BuyPrice'][0]))
                # print(((ticker.last*b['Position'][i] - b['BuyPrice'][i]*b['Position'][i])))

                b['CurrentPrice'][i] = ticker.last
                b['Change'][i] = (ticker.last - b['BuyPrice'][0])
                # b['Change (%)'][i] = (((ticker.last - x['BuyPrice'][0]))/x['BuyPrice'][0])*100
                b['P&L'][i] = ((ticker.last*b['Position'][i] - b['BuyPrice'][i]*b['Position'][i]))
        except:
            pass
    b = b.to_json(orient='records')
    ib.disconnect()
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def top_bar(request):

    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "topbar"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        
        df = pd.DataFrame()
        quandl.ApiConfig.api_key = 'Yc9EU38DA1ry-YHirE6Y'

        df = quandl.get('PERTH/AUD_USD_D')
        AUD_USD_D = df.tail(1)['Bid High'][0]
        AUD_USD_D_Change = round(df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0] , 3)
        AUD_USD_D_PChange = round(((df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0])/df.tail(1)['Bid High'][0])*100 , 2)

        df = quandl.get('PERTH/GOLD_USD_D')
        GOLD_USD_D = df.tail(1)['Bid High'][0]
        GOLD_USD_D_Change = round(df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0] , 3)
        GOLD_USD_D_PChange = round(((df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0])/df.tail(1)['Bid High'][0])*100 , 2)

        df = quandl.get('PERTH/SLVR_USD_D')
        SLVR_USD_D = df.tail(1)['Bid High'][0]
        SLVR_USD_D_Change = round(df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0] , 3)
        SLVR_USD_D_PChange = round(((df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0])/df.tail(1)['Bid High'][0])*100 , 2)

        df = quandl.get('PERTH/USD_JPY_D')
        USD_JPY_D = df.tail(1)['Bid High'][0]
        USD_JPY_D_Change = round(df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0] , 3)
        USD_JPY_D_PChange = round(((df.tail(1)['Bid High'][0] - df.tail(1)['Bid Low'][0])/df.tail(1)['Bid High'][0])*100 , 2)


        df = quandl.get('NASDAQOMX/NQUSB')
        df = df.fillna(0)
        NASDAQ = df.tail(1)['Index Value'][0]
        NASDAQ_Change = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0])/df.tail(1)['Index Value'][0] ) , 3)
        NASDAQ_PChange = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0])/df.tail(1)['Index Value'][0] )*100 , 2)


        df = quandl.get('NASDAQOMX/NQGI')
        NQGI = df.tail(1)['Index Value'][0]
        NQGI_Change = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0])/df.tail(1)['Index Value'][0] ) , 3)
        NQGI_PChange = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0])/df.tail(1)['Index Value'][0] )*100 , 2)

        df = quandl.get('NASDAQOMX/NQASIA')
        NQASIA = df.tail(1)['Index Value'][0]
        NQASIA_Change = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0])/df.tail(1)['Index Value'][0] ) , 3)
        NQASIA_PChange = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0])/df.tail(1)['Index Value'][0] )*100 , 2)

        df = quandl.get('NASDAQOMX/NQIN')
        NQIN = df.tail(1)['Index Value'][0]
        NQIN_Change = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0])/df.tail(1)['Index Value'][0] ) , 3)
        NQIN_PChange = round(((df.tail(1)['Index Value'][0] - df.tail(2)['Index Value'][0])/df.tail(1)['Index Value'][0] )*100 , 2)


        LIST = [ {'AUD_USD_D' : AUD_USD_D} , {'AUD_USD_D_Change' : AUD_USD_D_Change}  , {'AUD_USD_D_PChange' : AUD_USD_D_PChange}   , {'GOLD_USD_D' : GOLD_USD_D} , {'GOLD_USD_D_Change' : GOLD_USD_D_Change}  , {'GOLD_USD_D_PChange' : GOLD_USD_D_PChange} , {'SLVR_USD_D' : SLVR_USD_D} , {'SLVR_USD_D_Change' : SLVR_USD_D_Change}  , {'SLVR_USD_D_PChange' : SLVR_USD_D_PChange} , {'USD_JPY_D' : USD_JPY_D} , {'USD_JPY_D_Change' : USD_JPY_D_Change}  , {'USD_JPY_D_PChange' : USD_JPY_D_PChange} , {'NASDAQ' : NASDAQ} , {'NASDAQ_Change' : NASDAQ_Change}  , {'NASDAQ_PChange' : NASDAQ_PChange}  , {'NQGI' : NQGI} , {'NQGI_Change' : NQGI_Change}  , {'NQGI_PChange' : NQGI_PChange}   , {'NQASIA' : NQASIA} , {'NQASIA_Change' : NQASIA_Change}  , {'NQASIA_PChange' : NQASIA_PChange}  , {'NQIN' : NQIN} , {'NQIN_Change' : NQIN_Change}  , {'NQIN_PChange' : NQIN_PChange} ]

        pickobj = pickle.dumps(LIST)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        LIST  = pickle.loads(r.get(hash))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")







def sbox1(request):

    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "sbox1"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        
        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=JPY&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        USD_JPY = df_list.iloc[4]['Realtime Currency Exchange Rate']
        # print(USD_JPY)

        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=INR&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        USD_INR = df_list.iloc[4]['Realtime Currency Exchange Rate']
        # print(USD_INR)

        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=AUD&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        USD_AUD = df_list.iloc[4]['Realtime Currency Exchange Rate']
        # print(USD_AUD)

        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=EUR&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        USD_EUR = df_list.iloc[4]['Realtime Currency Exchange Rate']
        # print(USD_EUR)

        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=GBP&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        USD_GBP = df_list.iloc[4]['Realtime Currency Exchange Rate']
        # print(USD_GBP)


        LIST = [ {'USD_JPY' : USD_JPY} , {'USD_INR' : USD_INR} , {'USD_AUD' : USD_AUD} , {'USD_EUR' : USD_EUR} , {'USD_GBP' : USD_GBP} ]

        pickobj = pickle.dumps(LIST)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        LIST  = pickle.loads(r.get(hash))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")




def sbox2(request):

    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "sbox2"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")


        url = 'https://www.alphavantage.co/query?function=SECTOR&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        df = df_list['Rank A: Real-Time Performance']
        Financials = df[4]
        HealthCare = df[5]
        Industrials = df[6]
        InformationTechnology = df[8]
        Communication = df[0]



        LIST = [ {'Financials' : Financials}  , {'HealthCare' : HealthCare}  , {'Industrials' : Industrials}  , {'InformationTechnology' : InformationTechnology}  , {'Communication' : Communication}]

        pickobj = pickle.dumps(LIST)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        LIST  = pickle.loads(r.get(hash))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def sbox3(request):

    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "sbox3"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        
        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTC&to_currency=CNY&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        BTC = df_list.iloc[4]['Realtime Currency Exchange Rate']
        # print(BTC)


        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=ETH&to_currency=CNY&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        ETH = df_list.iloc[4]['Realtime Currency Exchange Rate']
        # print(ETH)


        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=ETC&to_currency=GBP&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        ETC = df_list.iloc[4]['Realtime Currency Exchange Rate']
        # print(ETC)


        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XMR&to_currency=GBP&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        XMR = df_list.iloc[4]['Realtime Currency Exchange Rate']
        # print(XMR)


        url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTG&to_currency=GBP&apikey=DUY16G6UHKSMRRKZ'
        html = requests.get(url).content
        df_list = pd.read_json(html)
        BTG = df_list.iloc[4]['Realtime Currency Exchange Rate']
        # print(BTG)


        LIST = [{'BTC' : BTC} , {'ETH' : ETH} , {'ETC' : ETC} , {'XMR' : XMR} , {'BTG' : BTG}]

        pickobj = pickle.dumps(LIST)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        LIST  = pickle.loads(r.get(hash))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")





def Mutual_Funds(request):

    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "lbox1"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")


        df = quandl.get('AMFI/143351')
        IDBI = df.tail(1)['Net Asset Value'][0]
        IDBI_Change = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])) , 3)
        IDBI_PChange = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])/df.tail(1)['Net Asset Value'][0] )*100 , 2)


        df = quandl.get('AMFI/113076')
        ICICI = df.tail(1)['Net Asset Value'][0]
        ICICI_Change = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])) , 3)
        ICICI_PChange = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])/df.tail(1)['Net Asset Value'][0] )*100 , 2)


        df = quandl.get('AMFI/126963')
        TATA = df.tail(1)['Net Asset Value'][0]
        TATA_Change = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])) , 3)
        TATA_PChange = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])/df.tail(1)['Net Asset Value'][0] )*100 , 2)


        df = quandl.get('AMFI/100033')
        Aditya_Birla = df.tail(1)['Net Asset Value'][0]
        Aditya_Birla_Change = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0]) ) , 3)
        Aditya_Birla_PChange = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])/df.tail(1)['Net Asset Value'][0] )*100 , 2)


        df = quandl.get('AMFI/100868')
        HDFC = df.tail(1)['Net Asset Value'][0]
        HDFC_Change = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])) , 3)
        HDFC_PChange = round(((df.tail(1)['Net Asset Value'][0] - df.tail(2)['Net Asset Value'][0])/df.tail(1)['Net Asset Value'][0] )*100 , 2)


        LIST = [{'IDBI' : IDBI} , {'IDBI_Change' : IDBI_Change}  , {'IDBI_PChange' : IDBI_PChange}  , {'ICICI' : ICICI} , {'ICICI_Change' : ICICI_Change}  , {'ICICI_PChange' : ICICI_PChange}  , {'TATA' : TATA} , {'TATA_Change' : TATA_Change}  , {'TATA_PChange' : TATA_PChange}   , {'Aditya_Birla' : Aditya_Birla} , {'Aditya_Birla_Change' : Aditya_Birla_Change}  , {'Aditya_Birla_PChange' : Aditya_Birla_PChange}  , {'HDFC' : HDFC} , {'HDFC_Change' : HDFC_Change}  , {'HDFC_PChange' : HDFC_PChange} ]



        pickobj = pickle.dumps(LIST)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        LIST  = pickle.loads(r.get(hash))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def Futures(request):

    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "lbox2"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")


        df = quandl.get('MCX/GMU2018')
        IDBI = df.tail(1)['High'][0]
        IDBI_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        IDBI_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('MCX/ALMX2015')
        ICICI = df.tail(1)['High'][0]
        ICICI_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        ICICI_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('MCX/SIMJ2017')
        TATA = df.tail(1)['High'][0]
        TATA_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        TATA_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('MCX/PBMQ2017')
        Aditya_Birla = df.tail(1)['High'][0]
        Aditya_Birla_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        Aditya_Birla_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('MCX/CUMV2017')
        HDFC = df.tail(1)['High'][0]
        HDFC_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        HDFC_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        LIST = [{'Gold_Mini' : IDBI} , {'Gold_Mini_Change' : IDBI_Change}  , {'Gold_Mini_PChange' : IDBI_PChange}  , {'Aluminium_Mini' : ICICI} , {'Aluminium_Mini_Change' : ICICI_Change}  , {'Aluminium_Mini_PChange' : ICICI_PChange}  , {'Silver_Mini' : TATA} , {'Silver_Mini_Change' : TATA_Change}  , {'Silver_Mini_PChange' : TATA_PChange}   , {'Lead_Mini' : Aditya_Birla} , {'Lead_Mini_Change' : Aditya_Birla_Change}  , {'Lead_Mini_PChange' : Aditya_Birla_PChange}  , {'Copper_Mini' : HDFC} , {'Copper_Mini_Change' : HDFC_Change}  , {'Copper_Mini_PChange' : HDFC_PChange} ]

        pickobj = pickle.dumps(LIST)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        LIST  = pickle.loads(r.get(hash))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def ETF(request):

    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "lbox3"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")


        df = quandl.get('NSE/SETFGOLD')
        IDBI = df.tail(1)['High'][0]
        IDBI_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        IDBI_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('NSE/KOTAKNV20')
        ICICI = df.tail(1)['High'][0]
        ICICI_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])) , 3)
        ICICI_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('NSE/SETFNIF50')
        TATA = df.tail(1)['High'][0]
        TATA_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])) , 3)
        TATA_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('NSE/ICICISENSX')
        Aditya_Birla = df.tail(1)['High'][0]
        Aditya_Birla_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        Aditya_Birla_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        df = quandl.get('NSE/AXISGOLD')
        HDFC = df.tail(1)['High'][0]
        HDFC_Change = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0]) ) , 3)
        HDFC_PChange = round(((df.tail(1)['High'][0] - df.tail(2)['High'][0])/df.tail(1)['High'][0] )*100 , 2)


        LIST = [{'SBI_Gold' : IDBI} , {'SBI_Gold_Change' : IDBI_Change}  , {'SBI_Gold_PChange' : IDBI_PChange}  , {'Kotak_Mamc' : ICICI} , {'Kotak_MamcChange' : ICICI_Change}  , {'Kotak_Mamc_PChange' : ICICI_PChange}  , {'SBI_Nifty50' : TATA} , {'SBI_Nifty50_Change' : TATA_Change}  , {'SBI_Nifty50_PChange' : TATA_PChange}   , {'ICICI_Sensex' : Aditya_Birla} , {'ICICI_Sensex_Change' : Aditya_Birla_Change}  , {'ICICI_Sensex_PChange' : Aditya_Birla_PChange}  , {'Axis_Gold' : HDFC} , {'Axis_Gold_Change' : HDFC_Change}  , {'Axis_Gold_PChange' : HDFC_PChange} ]

        pickobj = pickle.dumps(LIST)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        LIST  = pickle.loads(r.get(hash))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def Exchange_rates(request):

    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "lbox4"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        LIST = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")


        df = quandl.get('BUNDESBANK/BBEX3_M_AUD_USD_CM_AC_A01')
        IDBI = df.tail(1)['Value'][0]
        IDBI_Change = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0]) ) , 3)
        IDBI_PChange = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0])/df.tail(1)['Value'][0] )*100 , 2)


        df = quandl.get('BUNDESBANK/BBEX3_M_JPY_USD_CA_AA_A02')
        ICICI = df.tail(1)['Value'][0]
        ICICI_Change = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0])) , 3)
        ICICI_PChange = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0])/df.tail(1)['Value'][0] )*100 , 2)


        df = quandl.get('BUNDESBANK/BBEX3_M_CNY_USD_CA_AC_A01')
        TATA = df.tail(1)['Value'][0]
        TATA_Change = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0]) ) , 3)
        TATA_PChange = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0])/df.tail(1)['Value'][0] )*100 , 2)


        df = quandl.get('BUNDESBANK/BBEX3_M_INR_USD_CA_AC_A01')
        Aditya_Birla = df.tail(1)['Value'][0]
        Aditya_Birla_Change = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0]) ) , 3)
        Aditya_Birla_PChange = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0])/df.tail(1)['Value'][0] )*100 , 2)


        df = quandl.get('BUNDESBANK/BBEX3_M_USD_EUR_BB_AC_A02')
        HDFC = df.tail(1)['Value'][0]
        HDFC_Change = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0]) ) , 3)
        HDFC_PChange = round(((df.tail(1)['Value'][0] - df.tail(2)['Value'][0])/df.tail(1)['Value'][0] )*100 , 2)


        LIST = [{'USD_AUD' : IDBI} , {'USD_AUD_Change' : IDBI_Change}  , {'USD_AUD_PChange' : IDBI_PChange}  , {'USD_JPY' : ICICI} , {'USD_JPY_Change' : ICICI_Change}  , {'USD_JPY_PChange' : ICICI_PChange}  , {'USD_CNY' : TATA} , {'USD_CNY_Change' : TATA_Change}  , {'USD_CNY_PChange' : TATA_PChange}   , {'USD_INR' : Aditya_Birla} , {'USD_INR_Change' : Aditya_Birla_Change}  , {'USD_INR_PChange' : Aditya_Birla_PChange}  , {'USD_EUR' : HDFC} , {'USD_EUR_Change' : HDFC_Change}  , {'USD_EUR_PChange' : HDFC_PChange} ]


        pickobj = pickle.dumps(LIST)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        LIST  = pickle.loads(r.get(hash))



    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def Mutual_Fundsgraph(request):
    name = request.GET.get('name')
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "lbox1graph" + str(name)
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)
    List = []

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        dictt = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        b = quandl.get(name)
        b['Date'] = b.index
        # print(b.columns)
        b = b[['Date' , 'Net Asset Value']]
        b['Date'] = b.index
        b['ts'] = b.Date.values.astype(np.int64)
        b['ts'] = b['ts']//1000000

        for index, row in b.iterrows():
            List.append([ row['ts'] , row['Net Asset Value'] ])
        dictt  = {'data' : List}

        pickobj = pickle.dumps(dictt)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        dictt  = pickle.loads(r.get(hash))


    a = json.dumps(dictt  , indent=4)
    return HttpResponse(a,  content_type="application/json")



def Futuresgraph(request):
    name = request.GET.get('name')
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "lbox2graph" + str(name)
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)
    List = []

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        dictt = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        b = quandl.get(name)
        b['Date'] = b.index
        b = b[['Date' , 'Close']]
        b['Date'] = b.index
        b['ts'] = b.Date.values.astype(np.int64)
        b['ts'] = b['ts']//1000000

        for index, row in b.iterrows():
            List.append([ row['ts'] , row['Close'] ])
        dictt  = {'data' : List}

        pickobj = pickle.dumps(dictt)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        dictt  = pickle.loads(r.get(hash))


    a = json.dumps(dictt  , indent=4)
    return HttpResponse(a,  content_type="application/json")



def ETFgraph(request):
    name = request.GET.get('name')
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "lbox3graph" + str(name)
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)
    List = []

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        dictt = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        b = quandl.get(name)
        b['Date'] = b.index
        b = b[['Date' , 'Close']]
        b['Date'] = b.index
        b['ts'] = b.Date.values.astype(np.int64)
        b['ts'] = b['ts']//1000000

        for index, row in b.iterrows():
            List.append([ row['ts'] , row['Close'] ])
        dictt  = {'data' : List}

        pickobj = pickle.dumps(dictt)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        dictt = pickle.loads(r.get(hash))


    a = json.dumps(dictt  , indent=4)
    return HttpResponse(a,  content_type="application/json")



def Exchange_ratesgraph(request):
    name = request.GET.get('name')
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "lbox4graph" + str(name)
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)
    List = []

    if(r.get(hash)):
        print("LOADED FROM CACHE")
        dictt = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        b = quandl.get(name)
        b['Date'] = b.index
        # print(b.head())
        b = b[['Date' , 'Value']]
        b['Date'] = b.index
        b['ts'] = b.Date.values.astype(np.int64)
        b['ts'] = b['ts']//1000000

        for index, row in b.iterrows():
            List.append([ row['ts'] , row['Value'] ])
        dictt  = {'data' : List}

        pickobj = pickle.dumps(dictt)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        dictt  = pickle.loads(r.get(hash))


    a = json.dumps(dictt  , indent=4)
    return HttpResponse(a,  content_type="application/json")






def quandl_universe(request):
    name = request.GET.get('name')
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "quandl" + str(name)
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")
        b = quandl.get(name)

        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        b  = pickle.loads(r.get(hash))

    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")






ALL_DF_FS = pd.read_csv('American_Financial_Statements.csv')

def LR_Compare(request):
    name = request.GET.get('name')
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "LR_Compare_" + str(name)
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b = pickle.loads(r.get(hash))
        b.round(2)

    else:
        print("LOADED FROM DB")

        cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
        cursor = cnxn.cursor()
        query = "select Symbol from American_Stocks_Sectors"
        a = pd.read_sql_query(query , cnxn)
        a = list(a['Symbol'])
        a = a[0:50]

        # a = ['AAPL' , 'IBM' , 'GOOG' , 'MSFT' , 'ABB' , 'ANF' , 'ACCO' , 'ADNT' , 'AFL']
        
        b = 'American'
        c = name
        x = []

        Final_DF = pd.DataFrame()

        for i in a:
            try:
                # ALL_DF_FS.loc[(ALL_DF_FS['Company_Symbol'] == i) & (ALL_DF_FS['Abbreviation'] == c)& (ALL_DF_FS['Fiscal_Period_Type'] == 'Annual')]
                # query = "http://www.finflock.com/api/?select=End_Date,Abbreviation_Value&fro="+ b +"_Fundamentals_Financial_Statements&where=Company_Symbol='"+ i +"' AND Abbreviation='"+ c +"' AND Fiscal_Period_Type='Annual'"
                # print(query)
                # html = requests.get(query).content
                # df = pd.read_json(html)
                df = ALL_DF_FS.loc[(ALL_DF_FS['Company_Symbol'] == i) & (ALL_DF_FS['Abbreviation'] == c)& (ALL_DF_FS['Fiscal_Period_Type'] == 'Annual')]
                df = df.sort_values(['End_Date'], ascending=True)
        
        
                
                Final_DF[i] =  list(df['Abbreviation_Value'])
                
        #        Final_DF['Date'] = df['End_Date']

                x.append(i)
            except:
                pass
            
        # print(Final_DF)
        try:
            Final_DF['Date'] = list(df['End_Date'])
        except:
            pass
        # print(Final_DF)  
        columns = ['Date']
        columns.extend(x)

        Final_DF = Final_DF[columns]
        Final_DF.index = Final_DF['Date']
        Final_DF.drop('Date' , axis=1 , inplace=True)
        b = Final_DF.T
        b['Symbol'] = b.index
        # print(b)



        cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
        cursor = cnxn.cursor()
        query = "select * from American_Stocks_Sectors"
        s = pd.read_sql_query(query , cnxn)
        c = pd.merge(b, s, how='inner')
        c['Prediction'] = 0

        for i in range( 0 , c.shape[0]):
            try:
                model = LinearRegression()
                model.fit(pd.DataFrame([1,2,3,4,5,6]), pd.DataFrame([c.iloc[i][0] , c.iloc[i][1] , c.iloc[i][2] , c.iloc[i][3] , c.iloc[i][4] , c.iloc[i][5] ]))
                c['Prediction'][i] = model.predict(7)[0][0]
            except:
                c['Prediction'][i] = 'NAN'

        b = c
        b.columns = ['Dec 2012' , 'Dec 2013' , 'Dec 2014' , 'Dec 2015' , 'Dec 2016' , 'Dec 2017' , 'Symbol' , 'Name' , 'MarketCap' , 'Sector' , 'Prediction']


        # b['Dec 2012'] = float(b['Dec 2012']).round(2)
        # b['Dec 2013'] = float(b['Dec 2013']).round(2)
        # b['Dec 2014'] = float(b['Dec 2014']).round(2)
        # b['Dec 2015'] = float(b['Dec 2015']).round(2)
        # b['Dec 2016'] = float(b['Dec 2016']).round(2)
        # b['Dec 2017'] = float(b['Dec 2017']).round(2)
        b['Signal'] = 0
        for i in range( 0 , b.shape[0]):
            try:
                if b['Prediction'][i] - float(b['Dec 2017'][i])  > 0:
                    # print("BUY")
                    b['Signal'][i] = "BUY"
                else:
                    # print("SELL")
                    b['Signal'][i] = "SELL"
            except:
                b['Signal'][i] = "NAN"





        b.drop(b.index[0] , inplace=True)
        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        # r.expire(hash ,2* 60*60*24)
        b  = pickle.loads(r.get(hash))
    b.columns = ['Dec_2012' , 'Dec_2013' , 'Dec_2014' , 'Dec_2015' , 'Dec_2016' , 'Dec_2017' , 'Symbol' , 'Name' , 'MarketCap' , 'Sector' , 'Prediction' , 'Signal']
    b.drop_duplicates(subset=['Symbol'] , inplace=True)
    # print(b)
    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")




def Scanner(request):
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "Scanner_Stockmaniacs"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")

        query = "https://www.stockmaniacs.net/freebies/nse-stock-screener/"
        html = requests.get(query).content
        df = pd.read_html(html)
        b = df[0]
        b = pd.DataFrame(b)
        b.drop(['GAP >1%' , 'OPEN STATUS'] , axis=1 , inplace=True)
        b.columns = ['Symbol' , 'Open' , 'High' , 'Low' , 'LTP' , 'Volume' , 'Per_Change' , 'Last_Close' , 'Short_Trend' , 'Long_Trend']

        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        b  = pickle.loads(r.get(hash))

    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")



def Sector(request):
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "Sector"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")

        cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
        cursor = cnxn.cursor()
        query = "select * from American_Stocks_Sectors"
        b = pd.read_sql_query(query , cnxn)
        b.drop(b.index[0] , inplace=True)

        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        b  = pickle.loads(r.get(hash))
        

    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


def Sector_I(request):
    r = redis.StrictRedis(host='192.168.1.195', port=6380, db=5)
    SQL_String = "Sector_I"
    hash = hashlib.sha256(SQL_String.encode('utf-8')).hexdigest()
    # print(hash)
    # r.delete(hash)


    if(r.get(hash)):
        print("LOADED FROM CACHE")
        b = pickle.loads(r.get(hash))

    else:
        print("LOADED FROM DB")

        sheets = ['Automobile',
        'Banking&Finance',
        'Construction&Cements',
        'Chemicals',
        'Conglomerates',
        'Cons Durable',
        'Cons Non Durable',
        'Engineering',
        'Food & beverages',
        'GOLD ETF',
        'Manufacuring',
        'Media',
        'Metals&Mining',
        'Miscellaneous',
        'Oil & Gas',
        'Pharma',
        'Retail & Real Estate',
        'Services',
        'Telecom',
        'Tobacco',
        'Technology',
        'utilities']

        Final_Df = pd.DataFrame()

        for i in sheets:
            df = pd.read_excel('nse sector wise.xlsx' , sheetname=i)
            df['Sector'] = i
            # print(df.shape)
            Final_Df = Final_Df.append(df)
            b = Final_Df[['Company Name' , 'Mkt Cap' , 'Sector' , 'Industry']]
            b.drop(b.index[0] , inplace=True)

        pickobj = pickle.dumps(b)
        r.set(hash , pickobj)
        r.expire(hash ,2* 60*60*24)
        b  = pickle.loads(r.get(hash))

    b = b.to_json(orient='records')
    jsonfiles = json.dumps(json.loads(b)  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")




# def Login(request):
#     username = request.GET.get('username')
#     password = request.GET.get('password')

#     cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
#     cursor = cnxn.cursor()
#     query = "select * from R_Login3"

#     b = pd.read_sql_query(query , cnxn)
#     b = pd.DataFrame(b)

#     if ( (b.loc[(b['Username'] == username) & (b['PasswordHash'] == password)]).shape[0] > 0 ):
#         result = "True"
#     else:
#         result = "False"


#     return HttpResponse(result,  content_type="application/json")



# def Login(request):
#     username = request.GET.get('username')
#     password = request.GET.get('password')

#     cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
#     cursor = cnxn.cursor()
#     query = "select * from R_Login3"

#     b = pd.read_sql_query(query , cnxn)
#     b = pd.DataFrame(b)

#     if ( (b.loc[(b['Username'] == username) & (b['PasswordHash'] == password)]).shape[0] > 0 ):
#         result = "True"
#     else:
#         result = "True"


#     return HttpResponse(result,  content_type="application/json")



def Login(request):
    username = request.GET.get('username')

    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "select * from Dashboard_Login"

    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)

    b = b.loc[ (b['Username'] == username)].tail(1)
    LIST = [ {'Name' : b['Name'].iloc[0]} , {'Password' : b['PasswordHash'].iloc[0]} , {'Username' : b['Username'].iloc[0]} , {'Email' : b['Email'].iloc[0]}, {'CSS_Color' : b['CSS_Color'].iloc[0]} ]
    print(LIST)
    jsonfiles = json.dumps(LIST  , indent=4)
    return HttpResponse(jsonfiles,  content_type="application/json")


# def Register(request):
#     name = request.GET.get('name')
#     email = request.GET.get('email')
#     username = request.GET.get('username')
#     password = request.GET.get('password')


#     cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
#     cursor = cnxn.cursor()
#     cursor.execute("INSERT INTO R_Login3(Name , Email, Username , PasswordHash )" "VALUES (?, ?, ?, ?)", (name , email, username , password))
#     cnxn.commit()

#     query = "select * from R_Login3"

#     b = pd.read_sql_query(query , cnxn)
#     b = pd.DataFrame(b)

#     if ( (b.loc[(b['Name'] == name) & (b['Email'] == email) & (b['Username'] == username) & (b['PasswordHash'] == password)]).shape[0] > 0 ):
#         result = "True"
#     else:
#         result = "False"


#     cursor.close()
#     cnxn.close()


#     return HttpResponse(result,  content_type="application/json")

def Register(request):
    name = request.GET.get('name')
    email = request.GET.get('email')
    username = request.GET.get('username')
    password = request.GET.get('password')
    color = request.GET.get('color')
    IB = request.GET.get('IB')


    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    

    query = "select * from Dashboard_Login"

    b = pd.read_sql_query(query , cnxn)
    b = pd.DataFrame(b)

    if ( (b.loc[ (b['Email'] == email) ].shape[0] >= 1 )):
        result = "EmailE"

    elif ( (b.loc[  (b['Username'] == username)].shape[0] >= 1) ):
        result = "UsernameE"


    elif ( (b.loc[ (b['Email'] == email) & (b['Username'] == username)].shape[0] == 0) ):
        cursor.execute("INSERT INTO Dashboard_Login(Name , Email, Username , PasswordHash , IB, CSS_Color)" "VALUES (?, ?, ?, ? , ?, ?)", (name , email, username , password , IB, color))
        cnxn.commit()
        result = "Success"
        


    cursor.close()
    cnxn.close()


    return HttpResponse(result,  content_type="application/json")
'''